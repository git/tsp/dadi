#!/bin/bash

echo 'This install script has been deprecated, please use "$ cmake .; make ; make install" instead'

function fail() {
	echo "Installation failed"
	rm -rf $DEST/*
	exit -1
}

function usage() {
	echo "
 Usage: $0 [options]
    -f  Force install even if destination dir is empty
    -d  Destination directory (defaults to /opt/dadi)
    -D  Generate PDF documentation (requires latex)
    -h  Show this message
"
}

DEST="/opt/dadi"
FORCE_INSTALL=0
INSTALL_DOC=0
while getopts "hfd:D" options; do
	case $options in
		f ) FORCE_INSTALL=1;;
		d ) DEST=$OPTARG;;
		D ) INSTALL_DOC=1;;
		h ) usage && exit -1;;
		\?) usage && exit -1;;
		* ) usage && exit -1;;
	esac
done

#
# Handle the various install dir cases
# Only go on if the build was successful
#
if [ -d $DEST -a ${FORCE_INSTALL} -eq 0 ]
then
	echo "Installation dir $DEST exists, use -f to overwrite the previous installation"
	exit -1
elif [ -d $DEST -a ${FORCE_INSTALL} -eq 1 ]
then
	rm -rf $DEST/* || (echo "Could not delete previous install, exiting" ; exit -1)
fi

if [ ! -d $DEST -o ! -w $DEST ]
then
	mkdir -p $DEST 2> /dev/null || (echo "Not a valid destination directory: $DEST"; exit -1)
fi

HDEST=$DEST/src
CDEST=$DEST/src
LDEST=$DEST/lib
DDEST=$DEST/doc
EDEST=$DEST/example
TDEST=$DEST/tmpl
SDEST=$DEST/scripts
PWDEST=$DEST/wrapper/perl5

mkdir $HDEST || exit -1
mkdir $LDEST || exit -1
mkdir $DDEST || exit -1
mkdir $EDEST || exit -1
mkdir $TDEST || exit -1
mkdir $SDEST || exit -1
mkdir -p $PWDEST || exit -1

if [ ${INSTALL_DOC} -eq 1 ]
then
	echo "Building documentation"
	cd doc
	make
	cd ..
fi

DFILES="doc/dadi.pdf"
RFILES="bb_dumper.rb js_dumper.rb perl_dumper.rb dadi.rb data_tree.rb node.rb smi_parser.rb ipc_clean.sh"
CFILES="c/dadi.c c/dadi_snmp_tables.c c/dadi_snmp.c c/dadi_snmp_trap.c"
HFILES="c/dadi.h c/dadi_snmp.h c/dadi_snmp_tables.h c/dadi_snmp_trap.h"
TFILES="tmpl/bb.yaml"
EFILES="example/test.c example/Makefile"
SFILES="scripts/coverage.rb scripts/mib_gen.sh scripts/package.sh"
PWFILES="wrapper/perl5/libmib.i wrapper/perl5/Makefile"
SPECFILES="dadi.spec.in"

for i in $RFILES $HFILES $TFILES $PWFILES
do
	if [ ! -e $i ]
	then
		echo "Missing required file $i, exiting"
	fi
done

[ -f VERSION ] && cp VERSION $DEST  || fail
cp $RFILES $DEST  || fail
cp $TFILES $TDEST || fail
cp $HFILES $HDEST || fail
cp $CFILES $CDEST || fail
[ ${INSTALL_DOC} -eq 1 ] && (cp $DFILES $DDEST || fail)
cp $EFILES $EDEST || fail
cp -r $SFILES $SDEST || fail
cp $PWFILES $PWDEST || fail
cp $SPECFILES $DEST || fail

chmod +x $DEST/dadi.rb
chmod +x $DEST/ipc_clean.sh

echo "export RUBYLIB=$DEST" >> $DEST/dadienv
echo "export DADI_HOME=$DEST" >> $DEST/dadienv
echo 'export DADI_VERSION=$(cat ${DADI_HOME}/VERSION)' >> $DEST/dadienv
echo "export PATH=$PATH:$DEST:$SDEST" >> $DEST/dadienv
echo "export RUBYOPT=rubygems" >> $DEST/dadienv

echo "Installation successful"
