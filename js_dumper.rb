# js_dumper: dumps a javascript variable
# Copyright (C) 2007  Frederic Jansou, Frederik Deweerdt
# Copyright (C) 2008  S�bastien Ducloux
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program; if not, write to the Free Software Foundation,
# Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

require 'data_tree.rb'
require 'celsoft.com/template'
require 'yaml'

class Js_dumper
  attr_reader :jsfile
  def initialize(options)
    @options = options
    @tmpl = Template::Document.new()
    @templates=YAML::load(File.open("#{ENV['DADI_HOME']}/tmpl/javascript.tmpl"))
    @dest = options[:dest]
  end#initialize

  def dest_folders
    return [];
  end#dest_folders

  def prolog
    @jsfile = File.new("#{@dest}/sorted_elements.js", "w+")
    @jsfile.puts get_template("dadi_class")
    @refreshablefile = File.new("#{@dest}/refreshable.js", "w+")
    @refreshablefile.puts get_template("refreshable_start_class")
  end#end prolog

  def get_template(name, default=nil)
    begin
      @tmpl.load(@templates[name].clone)
      return @tmpl.output
    rescue
      raise if default == nil
      return default
    end
  end#get_template

  def parse_desc(node)
    h = Hash.new
    node["description"].split("%MDD_APP").each do
      |f|
      f =~ /(\w+) (\w+) *= *(.*)%!MDD_APP/

      if $1 == "IHM" then
        #
        # If we've already got an element with this name in the hash,
        # don't overwrite it, push it in an array instead
        #
        key = $2
        value = $3
        if h[key] != nil then
          if h[key].kind_of?(Array)
            a = h[key]
          else
            a = [h[key]]
          end#if
          a << value.gsub(/^\s*/, "").gsub(/\s*$/, "")
          h[key] = a
        else
          h[key] = value.gsub(/^\s*/, "").gsub(/\s*$/, "")
        end#if
      end#if
    end
    node["description"] = node["description"].gsub(/%MDD_APP(.*)!MDD_APP/,"")
    return h
  end#parse_desc

  def split_hash(ihm_hash)

    #If there is no "tab" field, the oid will not be displayed on IHM,
    #so we delete the node from the tree
    return false if ihm_hash["tab"] == nil

    #if the "tab" field exists but there is no multiple instance...
    #we don't have to split them!
    return nil if !ihm_hash["tab"].kind_of?(Array)

    new_nodes = []

    # Sanity check, group, fieldset and position must have the same size
    sizes = []
    [ "tab", "field", "position"].each do
      |f|
      sizes << (ihm_hash[f] == nil ? 0 : ihm_hash[f].size)
    end#each
    raise "Insane IHM directive #{n}" if sizes.uniq.size != 1

    # Create as much new nodes as we found "tab" directives
    Range.new(0, sizes[0]-1).each do
      |i|
      new_node = Hash.new
      ihm_hash.keys.each do
        |k|
        if ihm_hash[k].kind_of?(Array)
          new_node[k] = ihm_hash[k][i]
        else
          new_node[k] = ihm_hash[k]
        end#if
      end#each
      new_nodes << new_node
    end#each
    return new_nodes
  end#split_hash

  #
  # Finds if n needs to be splited
  # if yes, generated nodes are contat'ed to nodes_to_add and true
  # is returned
  # otherwhise, the node is enriched with description's parsing and
  # false is returned
  #
  def find_and_merge_duplicates(n, nodes_to_add)

    ihm_hash = parse_desc(n)

    # This node may need to be splitted
    nodes = split_hash(ihm_hash)

    #there is no "tab" field
    return true if nodes == false

    # If we need to split it ...
    if nodes != nil
      nodes.each { |node| node.merge!(n) }
      # ... the the nodes for later merge
      nodes_to_add.concat(nodes)
      return true
    else
      # ... no need to split, merge as-is
      n.merge!(ihm_hash)
      return false
    end#if
  end#find_and_merge_duplicates

  def dump_enums(tree)
    return if tree.nodes.size == 0
    res = { 'fill_enums' => '', 'init_enums' => '' }
    #Generate the enums defined in the MIB
    tree.enums.each do
      |e|
      enum_pairs = ""
      e["names"].each { |v|
        @tmpl.data = {}
        @tmpl.data["name"] = e["name"]
        @tmpl.data["value_name"] = v
        @tmpl.data["value_nb"] = e["values"][v]
        res["fill_enums"] += get_template("fill_enums")
      }
      @tmpl.data["fill_enums"] = res["fill_enums"]
      res["init_enums"] += get_template("init_enums")
    end#done
    @tmpl.data = {}
    return res
  end#dump_enums

  def dump_datatree(tree, jsname)
    @tmpl.data = dump_enums(tree)
    @tmpl.data["js_name"] = jsname
    #@jsfile.puts get_template("dadi_class")

    nodes_to_add = []
    nodes_to_delete = []

    tree.nodes.each do
      |n|
      n["js_name"] = jsname
      n["default"] = '""'
      tree.enums.each do
        |e|
        if e["name"] == n["name"] then
          n["range"] = "enum"
          n["default"] = "#{e["name"]}_value"
        end #if
      end#each
      nodes_to_delete << n if find_and_merge_duplicates(n, nodes_to_add)
    end#each

    nodes_to_delete.each do
      |n|
      tree.nodes.delete(n)
    end

    #The multiple instances of an OID are added to the tree.
    tree.nodes.concat(nodes_to_add)
		
		tables_to_delete = []
    tree.tables.each do
      |t|
      t["min"] = 0
      t["max"] = t["length"].to_i - 1
      t["js_name"] = jsname

      nodes_to_add = []
      nodes_to_delete = []

      t["columns"].each do
        |n|
        n["js_name"] = jsname
        n["default"] = '""'
        n["parent_name"] = t["name"]
        n["index"] = t["length"]
        tree.enums.each do
          |e|
          if e["name"] == n["name"] then
            n["range"] = "enum"
            n["default"] = "#{e["name"]}_value"
          end #if
        end#each
      nodes_to_delete << n if find_and_merge_duplicates(n, nodes_to_add)
      end#each

      nodes_to_delete.each do
        |n|
        t["columns"].delete(n)
      end

      t["columns"].concat(nodes_to_add)
			#if no column of a table is displayed in HMI, delete this table from the tree
      tables_to_delete << t if t['columns'].size == 0

    end#each
		
		tables_to_delete.each do
			|t|
			tree.tables.delete(t)
		end#each
		
    @tmpl.data["vars"] = tree.nodes if tree.nodes.size > 0
    @tmpl.data["tables"] = tree.tables if tree.tables.size > 0
    @jsfile.puts get_template("add_param")
    @refreshablefile.puts get_template("refreshable_class").gsub(/^\s*/, "").gsub(/\s*$/, "")
  end#dump_datatree

  def epilog
    @jsfile.puts get_template("end_function")
    @refreshablefile.puts get_template("refreshable_end_class")
  end#epilog

end#class Js_dumper
