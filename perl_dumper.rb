# perl_dumper.rb: dumps a perl variables
# Copyright (C) 2007  Frederic Jansou
# Copyright (C) 2007  S�bastien Ducloux
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program; if not, write to the Free Software Foundation,
# Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

require 'data_tree.rb'
require 'celsoft.com/template'
require 'yaml'

class Perl_dumper
	attr_reader :perlfile, :language_base_file 
  
  def Perl_dumper.add_options(optparser, options)
    optparser.on("-l","--language-base-file=FILE",String,"Language base file"){ |options[:language_base_file]| }
  end
  
	def initialize(options)
          @options = options
          @language_base_file = options[:language_base_file]
          @tmpl = Template::Document.new()
          @templates=YAML::load(File.open("#{ENV['DADI_HOME']}/tmpl/perl.tmpl"))
          @dest = options[:dest]
	end#initialize

	def dest_folders
		return [];
	end#dest_folders

	def prolog
  
          @perlfile = File.new("#{@dest}/sorted_elements.pl", "w+")
          @languagefile = File.new("#{@dest}/en", "w+")
          @refreshablefile = File.new("#{@dest}/refreshable.pl", "w+")
          @refreshablefile.puts get_template("refreshable_start_class")

    if(language_base_file.kind_of?(String) && File.exist?(language_base_file) )
    
      @languagefile_base = File.new(language_base_file, "r")
      
      @languagefile_base.each do
        |line|
        @languagefile.puts line
     end
    end
	end#end prolog

	def get_template(name, default=nil)
		begin
			@tmpl.load(@templates[name].clone)
			return @tmpl.output
		rescue
			raise if default == nil
			return default
		end
	end#get_template

  #
  # Get all %MDD_APP IHM key=value %!MDD_APP
  # and return a h[key] = value hash
  #
  def parse_desc(node)
    h = Hash.new
    node["description"].split("%MDD_APP").each do
      |f|
      f =~ /(\w+) (\w+) *= *(.*)%!MDD_APP/

      if $1 == "IHM" then
        #
        # If we've already got an element with this name in the hash,
        # don't overwrite it, push it in an array instead
        #
        key = $2
        value = $3
        if h[key] != nil then
          if h[key].kind_of?(Array)
            a = h[key]
          else
            a = [h[key]]
          end#if
          a << value.gsub(/^\s*/, "").gsub(/\s*$/, "")
          h[key] = a
        else
	        h[key] = value.gsub(/^\s*/, "").gsub(/\s*$/, "")
        end#if
      end#if
    end
    node["description"] = node["description"].gsub(/%MDD_APP(.*)!MDD_APP/,"")
    return h
  end#parse_desc

  def split_hash(ihm_hash)

    #If there is no "tab" field, the oid will not be displayed on IHM,
    #so we delete the node from the tree

    return false if ihm_hash["tab"] == nil

    #if the "tab" field exists but there is no multiple instance...
    #we don't have to split them!
    return nil if !ihm_hash["tab"].kind_of?(Array)

    new_nodes = []

    # Sanity check, group, fieldset and position must have the same size
    sizes = []
    [ "tab", "field", "position"].each do
      |f|
      sizes << (ihm_hash[f] == nil ? 0 : ihm_hash[f].size)
    end#each
    raise "Insane IHM directive #{n}" if sizes.uniq.size != 1

    # Create as much new nodes as we found "tab" directives
    Range.new(0, sizes[0]-1).each do
      |i|
      new_node = Hash.new
      ihm_hash.keys.each do
        |k|
        if ihm_hash[k].kind_of?(Array)
          new_node[k] = ihm_hash[k][i]
        else
          new_node[k] = ihm_hash[k]
        end#if
      end#each
      new_nodes << new_node
    end#each
    return new_nodes
  end#split_hash

  #
  # Finds if n needs to be splited
  # if yes, generated nodes are contat'ed to nodes_to_add and true
  # is returned
  # otherwhise, the node is enriched with description's parsing and
  # false is returned
  #
  def find_and_merge_duplicates(n, nodes_to_add)

    ihm_hash = parse_desc(n)

    # This node may need to be splitted
    nodes = split_hash(ihm_hash)

    #there is no "tab" field
    return true if nodes == false

    # If we need to split it ...
    if nodes != nil
      nodes.each { |node| node.merge!(n) }
      # ... the the nodes for later merge
      nodes_to_add.concat(nodes)
      return true
    else
      # ... no need to split, merge as-is
      n.merge!(ihm_hash)
      return false
    end#if
  end#find_and_merge_duplicates

	def dump_datatree(tree, perlname)

		@tmpl.data = {}
		@tmpl.data["perl_name"] = perlname

    nodes_to_add = []
    nodes_to_delete = []

    tree.nodes.each do
      |n|
      nodes_to_delete << n if find_and_merge_duplicates(n, nodes_to_add)
    end#each node

		# We delete all the nodes they have to : ones with multiple instances or
		# ones with no instance of the "tab" field.
		nodes_to_delete.each do
			|n|
			tree.nodes.delete(n)
		end

		#The multiple instances of an OID are added to the tree.
    tree.nodes.concat(nodes_to_add)

    tables_to_delete = []
    tree.tables.each do
			|t|
      current_rowstatus_oid = nil
      t["columns"].each do
        |c|
        if c["name"] =~ /RowStatus/
          current_rowstatus_oid = c["oid"]
        end
      end
      raise "Table #{t['name']} has no RowStatus field" if current_rowstatus_oid == nil

      nodes_to_add = []
      nodes_to_delete = []
      i = 0

      t["columns"].each do
        |n|
        n["index"] = t["length"]
        n["col_no"] = i
        n["row_status_oid"] = current_rowstatus_oid
        nodes_to_delete << n if find_and_merge_duplicates(n, nodes_to_add)
        i += 1
      end#each

      nodes_to_delete.each do
        |n|
        t["columns"].delete(n)
      end

      t["columns"].concat(nodes_to_add)
			
			#if no column of a table is displayed in HMI, delete this table from the tree
      tables_to_delete << t if t["columns"].size == 0 

		end#each table
      
      tables_to_delete.each do
      	|t|
				tree.tables.delete(t)
      end#each

		@tmpl.data["vars"] = tree.nodes if tree.tables.size > 0
		@tmpl.data["tables"] = tree.tables if tree.tables.size > 0
		@perlfile.puts get_template("dadi_class")
    @languagefile.puts get_template("language_class").gsub(/^\s*/, "").gsub(/\s*$/, "")
    @refreshablefile.puts get_template("refreshable_class").gsub(/^\s*/, "").gsub(/\s*$/, "")
  end#dump_datatree

	def epilog
    @refreshablefile.puts get_template("refreshable_end_class")
	end#epilog

end#class Perl_dumper
