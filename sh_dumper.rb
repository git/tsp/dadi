# sh_dumper: dumps a shell setter (this for test purposes mostly)
# Copyright (C) 2007  Frederik Deweerdt
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program; if not, write to the Free Software Foundation,
# Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

require 'data_tree.rb'
require 'yaml'

class Sh_dumper
	attr_reader :shfile
	def initialize(options)
		@options = options
    @dadi_to_snmpset_tmpl = { "int" => "",
                              "uint" => "",
                              "chartab" => "test-",
                              "macaddr" => "\"00 00 00 00 00 0\"",
                              "ipaddr" => "0.0.0." }
    @dadi_to_snmpset_type = { "int" => "i",
                              "uint" => "u",
                              "chartab" => "s",
                              "macaddr" => "s",
                              "ipaddr" => "a" }
	end#initialize

  def dest_folders
    return [];
  end#dest_folders

  def prolog
    @shfile = File.new("#{File.basename(@options[:xml_file][0], ".xml")}_tester.sh", "w+")
  end#end prolog

	def dump_enums(tree)
	end#dump_enums

  def dump_node(n, index=nil)
    return if n["access"] != "readwrite"
    snmpset_type = @dadi_to_snmpset_type[n["type"]]
    raise "unknown type #{n['type']}" if snmpset_type == nil
    snmpset_value = @dadi_to_snmpset_tmpl[n["type"]]
    snmpset_value = "\""+ snmpset_value + "$1\""

    index_str = ".0"
    index_str = ".#{index}" if index != nil

    @shfile.puts "snmpset -v2c -c public localhost #{n["oid"]}#{index_str} #{snmpset_type} #{snmpset_value}"
    @shfile.puts "[ $? -eq 0 ] && value=$(snmpget -OvUe -v2c -c public localhost #{n["oid"]}#{index_str}|awk '{print $2;}'|sed 's/^\"//' | sed 's/\"$//')"
    @shfile.puts "[ $value != #{snmpset_value} ] && (echo 'Error on oid #{n["oid"]}') && exit"
  end#dump_node

  def dump_datatree(tree, jsname)
		tree.nodes.each do
			|n|
        dump_node(n)
		end#each
		tree.tables.each do
			|t|
         t["columns"].each do
           |n|
           t["range"].each do
             |i|
             dump_node(n, i)
           end#each
         end#each
		end#each
	end#dump_datatree

  def epilog
  end#epilog

end#class Sh_dumper
