# java_dumper: dumps Java Variables
#
# Copyright (C) 2006,2007,2008
# Frederik Deweerdt <frederik.deweerdt@gmail.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program; if not, write to the Free Software Foundation,
# Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

require 'data_tree.rb'
require 'celsoft.com/template'
require 'yaml'

class Java_dumper
  def initialize(options)
    @options = options
    @tmpl = Template::Document.new()
    @templates=YAML::load(File.open("#{ENV['DADI_HOME']}/tmpl/java.tmpl"))
  end#initialize

  def dest_folders
    return [];
  end#dest_folders

  def prolog
    #@javafile = File.new("#{File.basename(@options[:xml_file][0], ".xml")}.java", "w+")
    @javafile = File.new("DadiVars.java", "w+")
  end#end prolog

  def get_template(name, default=nil)
    begin
      @tmpl.load(@templates[name].clone)
      return @tmpl.output
    rescue
      raise if default == nil
      return default
    end
  end#get_template


  def dump_enums(tree)
    tree.enums.each do
      |e|
      enum_pairs = ""
      e["names"].each { |v|
        @tmpl.data = {}
        @tmpl.data["name"] = "#{e['name']}_#{v}"
        @tmpl.data["value"] = e["values"][v]
        enum_pairs += get_template("enum_pair")
      }
      @tmpl.data = e
      @tmpl.data["enum_pairs"] = enum_pairs
      @efile_string += get_template("enum_decl")
    end#done
  end#dump_enums

  #
  # Get all %MDD_APP IHM key=value %!MDD_APP
  # and return a h[key] = value hash
  #
  def parse_desc(node)
    h = Hash.new
    node["description"].split("%MDD_APP").each do
      |f|
      f =~ /(\w+) (\w+) *= *(.*)%!MDD_APP/
      if $1 == "IHM" then
        #
        # If we've already got an element with this name in the hash,
        # don't overwrite it, push it in an array instead
        #
        key = $2
        value = $3
        if h[key] != nil then
          if h[key].kind_of?(Array)
            a = h[key]
          else
            a = [h[key]]
          end#if
          a << value.gsub(/^\s*/, "").gsub(/\s*$/, "")
          h[key] = a
        else
          h[key] = value.gsub(/^\s*/, "").gsub(/\s*$/, "")
        end#if
      end#if
    end
    return h
  end#parse_desc

  def dump_datatree(tree, name)
    @javafile.puts get_template("dadivars_class_begin")
    tree.enums.each do
      |e|
      enum_pairs = ""
      e["names"].each { |v|
        @tmpl.data = {}
        @tmpl.data["name"] = "#{e['name']}_#{v}"
        @tmpl.data["value"] = e["values"][v]
        enum_pairs += get_template("enum_decl")
      }
      @javafile.puts enum_pairs
    end#each

    @javafile.puts get_template("dadivars_constructor_begin")

    tree.nodes.each do
      |n|
      @tmpl.data = n.merge!(parse_desc(n))
      @javafile.puts get_template("dadivar_decl")
    end#each

    @javafile.puts get_template("dadivars_constructor_end")
    @javafile.puts get_template("dadivars_class_end")
  end#dump_datatree

  def epilog
  end#epilog

end#class Java_dumper
