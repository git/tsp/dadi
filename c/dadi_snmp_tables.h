/*
    dadi_snmp_tables.h - SNMP tables definitions, used by the bb_dumper

    Copyright (C) 2006,2007,2008
    Frederik Deweerdt <frederik.deweerdt@gmail.com>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
    02110-1301 USA.
*/

#ifndef _DADI_SNMP_TABLE_H_
#define _DADI_SNMP_TABLE_H_

#endif /* _DADI_SNMP_TABLE_H_ */
