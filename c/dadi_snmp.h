/*
    dadi_snmp.h - dadi SNMP definitions, used by the bb_dumper

    Copyright (C) 2006,2007,2008
    Frederik Deweerdt <frederik.deweerdt@gmail.com>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
    02110-1301 USA.
*/

#ifndef __DADI_SNMP__
#define __DADI_SNMP__

#include <bb_core.h>

#include <net-snmp/net-snmp-config.h>
#include <net-snmp/net-snmp-includes.h>
#include <net-snmp/agent/net-snmp-agent-includes.h>

typedef void (*dadi_snmp_set_callback_t)(void *);
typedef void (*dadi_snmp_set_index_callback_t)(int i, void *);
typedef void *(*dadi_snmp_get_callback_t)(void);
typedef void *(*dadi_snmp_get_index_callback_t)(int i);

/** */
enum dadi_access {
	DADI_RO,
	DADI_RW,
};

/** */
enum dadi_type {
	DADI_INT,
	DADI_UINT,
	DADI_COUNTER,
	DADI_UINT64,
	DADI_CHARTAB,
	DADI_IPADDR,
	DADI_MACADDR,
};

/** Describes a variable handled by the DADI framework */
struct dadi_variable {
	void *value;
	char *name;
	dadi_snmp_set_callback_t set;
	dadi_snmp_set_index_callback_t set_index;
	dadi_snmp_get_callback_t get;
	dadi_snmp_get_callback_t get_P;
	dadi_snmp_get_index_callback_t get_index;
	int varno;
	const oid *oid;
	int oid_len;
	enum dadi_type type;
	enum dadi_access access;
	int size;
	S_BB_T *bb;
};

struct dadi_table_row {
	struct dadi_table *table;
	unsigned int index;
	/* null terminated dadi_variable list */
	struct dadi_variable ** variables;
};


/** Describes a table handled by the DADI framework */
struct dadi_table {
	char *name;
	int varno;
	const oid *oid;
	int oid_len;
	enum dadi_access access;
	int min_column;
	int max_column;
	struct dadi_variable *index;
	/* null terminated dadi_row list */
	struct dadi_table_row ** rows;
	unsigned int first_index;
	unsigned int nb_row;
};

static inline int dadi_get_len(struct dadi_variable *v)
{
	switch (v->type) {
		case DADI_INT:
			return sizeof(int);
		case DADI_CHARTAB:
		case DADI_IPADDR:
		case DADI_MACADDR:
			return v->size;
		default:
			assert(0);
	}
}

int dadi_access_to_snmp(enum dadi_access access);

int dadi_type_to_ASN(int dadi_type);

int register_snmp_callback(struct dadi_variable *scalar);
int register_snmp_table_callback(struct dadi_table *dtable);

int netsnmp_register_int_instance_with_cb(const char *name,
					  oid * reg_oid, size_t reg_oid_len,
					  struct dadi_variable *it,
					  Netsnmp_Node_Handler * subhandler);
#endif /* __DADI_SNMP__ */
