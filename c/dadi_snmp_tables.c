/*
    dadi_snmp_tables.c - SNMP tables handling code, used by the bb_dumper

    Copyright (C) 2006,2007,2008
    Frederik Deweerdt <frederik.deweerdt@gmail.com>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
    02110-1301 USA.

*/

#include <assert.h>

#include "dadi.h"
#include "dadi_snmp.h"
#include "dadi_snmp_tables.h"

static int
snmp_dadi_handler(netsnmp_mib_handler *handler,
		  netsnmp_handler_registration *reginfo,
		  netsnmp_agent_request_info *reqinfo,
		  netsnmp_request_info *requests);

int register_snmp_table_callback(struct dadi_table *dtable)
{
	netsnmp_table_data *table;
	netsnmp_table_row *row;
	int i;
	netsnmp_table_registration_info *table_info;

	table = netsnmp_create_table_data("data_table_test");

	netsnmp_table_data_add_index(table, ASN_INTEGER);

	for (i = dtable->first_index;
	     dtable->rows[i-dtable->first_index] != NULL;
	     i++) {
		row = netsnmp_create_table_data_row();
		netsnmp_table_row_add_index(row, ASN_INTEGER, &i, sizeof(i));
		row->data = dtable->rows[i-(dtable->first_index)];
		netsnmp_table_data_add_row(table, row);
	}

	table_info = SNMP_MALLOC_TYPEDEF(netsnmp_table_registration_info);

	netsnmp_table_helper_add_indexes(table_info, ASN_INTEGER, 0);
	table_info->min_column = dtable->min_column;
	table_info->max_column = dtable->max_column;

	/* The 3rd param of netsnmp_register_table_data is not
	 * declared as const... and it should (only a copy of it is
	 * performed)...  The following hack avoid warnings */
	union {
		const oid * const_oid;
		oid * oid;
	} remove_const = { .const_oid = dtable->oid } ;

	return netsnmp_register_table_data(netsnmp_create_handler_registration(
						dtable->name,
						snmp_dadi_handler,
						remove_const.oid,
						dtable->oid_len,
						HANDLER_CAN_RWRITE),
					   table, table_info);
}


static int
snmp_dadi_handler(netsnmp_mib_handler *handler,
		  netsnmp_handler_registration *reginfo,
		  netsnmp_agent_request_info *reqinfo,
		  netsnmp_request_info *requests)
{
	char chartab_tmp[2048];

	for ( ; requests; requests = requests->next) {
		netsnmp_table_row *row;
		netsnmp_table_request_info *table_info;
		struct dadi_table_row *dadi_row;
		struct dadi_table *table;
		struct dadi_variable *var;
		int idx;

		row  = netsnmp_extract_table_row(requests);
		if (!row)
			continue;

		table_info = netsnmp_extract_table_info(requests);
		if (!table_info)
			continue;

		dadi_row = row->data;
		if (!dadi_row)
			continue;

		table = dadi_row->table;
		if (!table)
			continue;

		idx = table_info->colnum - table->min_column;
		var = dadi_row->variables[idx];
		if (!var)
			continue;

		switch (reqinfo->mode) {
		case MODE_GET:
		{
			intptr_t integer_value;
			uintptr_t uinteger_value;
			uint64_t u64_value;
			struct counter64 c64;
			void *value = NULL;
			size_t size = var->size;
			dadi_evt(var->varno, DADI_EVENT_TYPE_GET);
			switch (var->type) {
			case DADI_INT:
				integer_value = (intptr_t)var->get_index(dadi_row->index);
				value = &integer_value;
				break;
			case DADI_UINT:
			case DADI_COUNTER:
				uinteger_value = (uintptr_t)var->get_index(dadi_row->index);
				value = &uinteger_value;
				break;
			case DADI_UINT64:
				u64_value = (uint64_t)var->get_index(dadi_row->index);
				c64.high = u64_value >> 32;
				c64.low = u64_value & 0xffffffff;
				value = &c64;
				break;
			case DADI_CHARTAB:
				value = var->get_index(dadi_row->index);
				size = strlen(value);
				break;
			case DADI_IPADDR:
			case DADI_MACADDR:
				value = var->get_index(dadi_row->index);
				break;
			}
			netsnmp_table_data_build_result(reginfo, reqinfo, requests,
							row, table_info->colnum,
							dadi_type_to_ASN(var->type),
							value, size);
			break;
		}
		case MODE_SET_RESERVE1:
		case MODE_SET_RESERVE2:
			break;
		case MODE_SET_ACTION:
		{
			void *set_value = NULL;
			void *dadi_evt_value;
			uint64_t u64_value;

			switch (var->type) {
			case DADI_INT:
			case DADI_UINT:
			case DADI_COUNTER:
				set_value = (void *)*(requests->requestvb->val.integer);
				dadi_evt_value = requests->requestvb->val.integer;
				break;
			case DADI_UINT64:
				u64_value = (uint64_t)requests->requestvb->val.counter64->high << 32
					    | requests->requestvb->val.counter64->low;
				set_value  = (void *)u64_value;
				dadi_evt_value = (void *)u64_value;
				break;
			case DADI_CHARTAB:
				strncpy(chartab_tmp, (char *)requests->requestvb->val.string,
					requests->requestvb->val_len);
				chartab_tmp[requests->requestvb->val_len] = '\0';
				set_value = chartab_tmp;
				dadi_evt_value = chartab_tmp;
				break;
			case DADI_IPADDR:
			case DADI_MACADDR:
				set_value = dadi_evt_value =
					requests->requestvb->val.string;
				break;
			}
			if (var->access != DADI_RW)
				return SNMP_ERR_READONLY;

			var->set_index(dadi_row->index, set_value);
			dadi_evt(var->varno, DADI_EVENT_TYPE_SET);
			break;
		}
		case MODE_SET_UNDO:
		case MODE_SET_COMMIT:
		case MODE_SET_FREE:
			break;
		default:
			break;
		}
		netsnmp_set_request_error(reqinfo, requests,
					  SNMP_ERR_NOERROR);
	}


	if (handler->next && handler->next->access_method) {
		return netsnmp_call_next_handler(handler, reginfo, reqinfo,
						 requests);
	}

	return SNMP_ERR_NOERROR;
}
