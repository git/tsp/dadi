#include <net-snmp/net-snmp-config.h>
#include <net-snmp/net-snmp-includes.h>
#include <net-snmp/agent/net-snmp-agent-includes.h>

#include "dadi_snmp.h"
#include "dadi_snmp_trap.h"

/* mandatory for an SNMPv2 notification trap */
static oid objid_snmptrap[] = { 1, 3, 6, 1, 6, 3, 1, 1, 4, 1, 0 };
static size_t objid_snmptrap_len = OID_LENGTH(objid_snmptrap);


int dadi_send_trap(oid *trap_oid, int trap_oid_len,
                   struct dadi_variable *scalars, int nb_vars)
{
	int i, size;
	netsnmp_variable_list *varlist = NULL;
	snmp_varlist_add_variable(&varlist,
			objid_snmptrap, objid_snmptrap_len,
			ASN_OBJECT_ID,
			(unsigned char *)trap_oid,
			trap_oid_len * sizeof(oid));

	for (i = 0; i < nb_vars; i++) {
		if (scalars[i].type == DADI_CHARTAB)
			size = strlen(scalars[i].get());
		else
			size = scalars[i].size;

		snmp_varlist_add_variable(&varlist,
				scalars[i].oid, scalars[i].oid_len,
				dadi_type_to_ASN(scalars[i].type),
				(unsigned char *)scalars[i].get_P(), size);
	}

	send_v2trap(varlist);
	snmp_free_varbind(varlist);

	return 0;
}

int dadi_add_trap_host(char *hostname, unsigned short port)
{
	static int inited = 0;
	if (!inited) {
		init_traps();
		inited = 1;
	}

	return create_trap_session(hostname, port, "public", SNMP_VERSION_2c, SNMP_MSG_TRAP2);
}
