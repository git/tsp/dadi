/*
    dadi.h - dadi C definitions, used by the bb_dumper

    Copyright (C) 2006,2007,2008
    Frederik Deweerdt <frederik.deweerdt@gmail.com>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
    02110-1301 USA.
*/

#ifndef _DADI_H_
#define _DADI_H_

#include <stddef.h>
#include <unistd.h>
#include <stdint.h>
#include <string.h>
#include <bb_core.h>
#include <bb_simple.h>

#define MAX_BBS 255

extern void register_mib_init(void);

typedef int  INTEGER;
typedef unsigned int Unsigned32;
typedef unsigned int Gauge32;
typedef unsigned int Counter32;
typedef int  Integer32;
typedef int  TruthValue;
typedef char *DisplayString;
typedef uint8_t IpAddress[4];
typedef uint8_t MacAddress[6];
typedef char *OctetString;
typedef int InetAddressType;
typedef char *InetAddress;
typedef unsigned int InetPortNumber;
typedef uint64_t Counter64;
typedef uint64_t CounterBasedGauge64;

/** maximum length of a parameter string */
#define MAX_STRING_SIZE 255

/** type defining the parameter strings that can be passed */
typedef unsigned char string_t[MAX_STRING_SIZE];

/** Different types that we can listen to */

enum dadi_event_type {
	DADI_EVENT_TYPE_GET,
	DADI_EVENT_TYPE_SET
};


/** definition of the callback that needs to be provided to the library.
 * @param var the variable that was changed (see enums.h)
 * @param data the value of the parameter. Must be filled if the operation is a read, is filled if operation is a write
 * @param size size of the data that has changed
 * @return 0 if the operation succeeded, -1 otherwise
 * @warning the data is local to the calling variable. So it must be copied somewhere else before returning.
 */
typedef int (*dadi_callback_t)(int var, enum dadi_event_type dadi_event_type);

/** @brief first initialisation function for the library.
 *
 * This function makes the pre-initialisation of the library. It allocates necessary strutures and so on.
 * @param theActivationCallback the activation callback that will be called when parameters are modified
 * @return 0 if the initialisation fails, or a pointer on the global configuration and counters
 */
void *dadi_first_init(dadi_callback_t theActivationCallback);

/**  * @brief Function for internal use, triggers the call to callbacks
 *
 * @param dadi_var the variable about to change
 * @param event_type the type of event happening
 *
 * @return if evt_type modifies a value: 1 whenever the value should be commited to the BB, 0 otherwise
 *         if evt_type doesn't modify a value: always returns 1
 **/
int dadi_evt(int dadi_var, enum dadi_event_type event_type);

/** @brief main dadi loop
 *
 * This function is the main dadi loop that processes all incoming
 * data. It never ends unless dadi_stop is called
 * @return 0 if the library closed successfully, -1 otherwise
*/
int dadi_mainloop(int);

/** @brief function that ends the main loop
 *
 * This function gives the main dadi loop an order to stop processing.
 * It then waits for 1s at maximum for the main loop to be stopped.
 * It returns 0 as soon as the main loop is stopped, or -1 if after one second,
 * the main loop is not stopped. In this case, there is a problem in the main loop.
 * This should never occur
 * @return 0 if the main loop successfully stops, -1 otherwise
 */
int dadi_terminate_request(void);

/** @brief Closes the library.
 *
 * This function frees all allocated data within the dadi library
 * This function must be called when the program using the library
 * exits. If -1 is returned, then an internl bug is present.
 *
 * @return 0 if the library closed successfully, -1 otherwise
 */
int dadi_stop(void);

/** informs whenever the dadi user is running in master mode (ie. is SNMP server) **/
/** !!! Carefull, after the call to dadi_mainloop(), a client can become a master */
extern int is_master;

struct dadi_allocator {
	void (*init)(void);
	void *(*malloc)(size_t);
	void *(*calloc)(size_t, size_t);
	void *(*realloc)(void *, size_t);
	char *(*dup)(const char *);
	void (*free)(void *);
	void (*fini)(void);
};

/* explicitely declare the visibility to be the same as mib.c's */
extern struct dadi_allocator *__dadi_allocator __attribute__ ((visibility ("default")));

struct mib_specific {
	int max_id;
	S_BB_T **(*register_bb) (void);
	int (*register_snmp)(void);
	/** Identifies uniquely the MIBS we've been built from, used to check against
	 * incompatible clients */
	const char *(*datatree_sha1)(void);
};

/** @brief Register a MIB.
 *
 * This function registers a MIB (autoamtically called by bb_dumper
 * generated code)
 *
 */
void dadi_register_mib (struct mib_specific *mib);


/** @brief Type to define the way to log.
 *
 * @param priority syslog like priority (see man syslog(3)).
 * @param file the filename where the has been done.
 * @param line the line where the has been done.
 * @param function the function where the has been done.
 * @param fmt printf like format.
 * @param ... the variable list of params
 *
 */
typedef void (*dadi_log_t) (int priority, const char *file, int line,
			    const char *function , const char *fmt, ...) __attribute__((format(printf,5,6)));

/** @brief Set the logging method.
 *
 * This function registers a logging function.
 * If never called, the default (logging on stderr) will be performed
 * @param event_type the type of event happening
 *
 * @returns the previous logging method.
 */
dadi_log_t dadi_set_loging_method(dadi_log_t new_dadi_log);

/** @brief Sets the client timeout
 *
 * This function sets the client timeout.
 * If never called, the default (1 second) is used.
 * the client timeout is teh timeout over wich a client is considered
 * as dead.
 * @param new_clients_timeout the new clients timeout
 *
 * @returns the previous logging method.
 */
void dadi_set_clients_timeout(const struct timeval *new_clients_timeout);

void dadi_set_mibdir(const char *path);
void dadi_set_cfgdir(const char *path);

#endif /* _DADI_H_ */
