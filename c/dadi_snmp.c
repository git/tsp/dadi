/*
    dadi_snmp.c - SNMP related code, used by the bb_dumper

    Copyright (C) 2006,2007,2008
    Frederik Deweerdt <frederik.deweerdt@gmail.com>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
    02110-1301 USA.
*/

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <assert.h>

#include <net-snmp/net-snmp-config.h>
#include <net-snmp/net-snmp-includes.h>
#include <net-snmp/agent/net-snmp-agent-includes.h>

#include "dadi_snmp.h"
#include "dadi.h"

/*
 * For some reason, this is not provided by the net-snmp libs
 * we had it here so that we can compile safely
 */
const char *make_tempfile(void);
const char *make_tempfile(void)
{
	static char name[32];
	int fd = -1;

	strcpy(name, get_temp_file_pattern());
	fd = mkstemp(name);
	if (fd >= 0) {
		close(fd);
		DEBUGMSGTL(("netsnmp_mktemp", "temp file created: %s\n", name));
		return name;
	}
	snmp_log(LOG_ERR, "netsnmp_mktemp: error creating file %s\n", name);
	return NULL;
}

int dadi_type_to_ASN(int dadi_type)
{
	switch (dadi_type) {
	case DADI_CHARTAB:
	case DADI_MACADDR:
		return ASN_OCTET_STR;
	case DADI_IPADDR:
		return ASN_IPADDRESS;
	case DADI_INT:
		return ASN_INTEGER;
	case DADI_UINT:
		return ASN_UNSIGNED;
	case DADI_COUNTER:
		return ASN_COUNTER;
	case DADI_UINT64:
		return ASN_COUNTER64;
	default:
		return -1;
	}
}

int dadi_access_to_snmp(enum dadi_access dadi_access)
{
	switch (dadi_access) {
	case DADI_RO:
		return HANDLER_CAN_RONLY;
	case DADI_RW:
		return HANDLER_CAN_RWRITE;
	default:
		return -1;
	}
}

static int scalar_handler(netsnmp_mib_handler *handler,
			  netsnmp_handler_registration *reginfo,
			  netsnmp_agent_request_info *reqinfo,
			  netsnmp_request_info *requests);

static int scalar_handler(netsnmp_mib_handler *handler,
			  netsnmp_handler_registration *reginfo,
			  netsnmp_agent_request_info *reqinfo,
			  netsnmp_request_info *requests)
{
	char chartab_tmp[2048];
	struct dadi_variable *var = handler->myvoid;

	for ( ; requests; requests = requests->next) {
		switch (reqinfo->mode) {
		case MODE_GET:
		{
			intptr_t integer_value;
			uintptr_t uinteger_value;
			uint64_t u64_value;
			struct counter64 c64;
			void *value = NULL;
			size_t size = var->size;
			dadi_evt(var->varno, DADI_EVENT_TYPE_GET);
			switch (var->type) {
			case DADI_INT:
				integer_value = (intptr_t)var->get();
				value = &integer_value;
				break;
			case DADI_UINT:
			case DADI_COUNTER:
				uinteger_value = (uintptr_t)var->get();
				value = &uinteger_value;
				break;
			case DADI_UINT64:
				u64_value = (uint64_t)var->get();
				c64.high = u64_value >> 32;
				c64.low = u64_value & 0xffffffff;
				value = &c64;
				break;
			case DADI_CHARTAB:
				value = (char *)var->get();
				size = strlen(value);
				break;
			case DADI_IPADDR:
			case DADI_MACADDR:
				value = var->get();
				break;
			}
			snmp_set_var_typed_value(requests->requestvb,
						 dadi_type_to_ASN(var->type),
						 value, size);

			break;
		}
		case MODE_SET_RESERVE1:
		case MODE_SET_RESERVE2:
			break;
			/*
			 * actually update the value the process as follows:
			 * 1) Call the registed callbacks
			 * 2) If everyone said OK, write the value (TODO)
			 * 3) return an error if someone actually
			 * disagreed with it (TODO)
			 */
		case MODE_SET_ACTION:
		{
			void *set_value = NULL;
			uint64_t u64_value;
			void *dadi_evt_value;
			switch (var->type) {
			case DADI_INT:
			case DADI_UINT:
			case DADI_COUNTER:
				set_value = (void *)*(requests->requestvb->val.integer);
				dadi_evt_value = requests->requestvb->val.integer;
				break;
			case DADI_UINT64:
				u64_value = (uint64_t)requests->requestvb->val.counter64->high << 32
					    | requests->requestvb->val.counter64->low;
				set_value  = (void *)u64_value;
				dadi_evt_value = (void *)u64_value;
				break;
			case DADI_CHARTAB:
				strncpy(chartab_tmp,
					(char *)requests->requestvb->val.string,
					requests->requestvb->val_len);
				chartab_tmp[requests->requestvb->val_len] = '\0';
				set_value = chartab_tmp;
				dadi_evt_value = chartab_tmp;
				break;
			case DADI_IPADDR:
			case DADI_MACADDR:
				set_value = dadi_evt_value =
					requests->requestvb->val.string;
				break;
			}
			if (var->access != DADI_RW)
				return SNMP_ERR_READONLY;

			var->set(set_value);
			dadi_evt(var->varno, DADI_EVENT_TYPE_SET);

			break;
		}
		case MODE_SET_UNDO:
		case MODE_SET_COMMIT:
		case MODE_SET_FREE:
			break;
		}
		netsnmp_set_request_error(reqinfo, requests,
					  SNMP_ERR_NOERROR);
	}

	if (handler->next && handler->next->access_method) {
		return netsnmp_call_next_handler(handler, reginfo, reqinfo,
						 requests);
	}

	return SNMP_ERR_NOERROR;
}

int register_snmp_callback(struct dadi_variable *scalar)
{
	netsnmp_handler_registration *reginfo;
	int ret;

	netsnmp_mib_handler *mib_handler =
		netsnmp_create_handler(scalar->name,
				       scalar_handler);

	mib_handler->myvoid = scalar;

	/* The 3rd param of netsnmp_handler_registration_create is not
	 * declared as const... and it should (only a copy of it is
	 * performed)...  The following hack avoid warnings */
	union {
		const oid * const_oid;
		oid * oid;
	} remove_const = { .const_oid =  scalar->oid } ;

	reginfo = netsnmp_handler_registration_create(scalar->name,
						      mib_handler,
						      remove_const.oid,
						      scalar->oid_len,
						      dadi_access_to_snmp(scalar->access));

	ret = netsnmp_register_instance(reginfo);
	return ret;
}
