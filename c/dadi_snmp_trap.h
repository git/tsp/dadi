#ifndef _DADI_SNMP_TRAP_H_
#define  _DADI_SNMP_TRAP_H_

int dadi_send_trap(oid *trap_oid, int trap_oid_len,
                   struct dadi_variable *scalars, int nb_vars);
int dadi_add_trap_host(char *hostname, unsigned short port);

#endif /*  _DADI_SNMP_TRAP_H_ */
