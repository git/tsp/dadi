/*
    dadi.c - dadi C code, used by the bb_dumper

    Copyright (C) 2006,2007,2008
    Frederik Deweerdt <frederik.deweerdt@gmail.com>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
    02110-1301 USA.
*/
#ifndef _GNU_SOURCE
# define _GNU_SOURCE
#endif
#include <sys/stat.h>
#include <fcntl.h>
#include <errno.h>
#include <limits.h>
#include <signal.h>
#include <string.h>
#include <sys/queue.h>
#include <sys/socket.h>
#include <sys/time.h>

#include <time.h>


#include <net-snmp/net-snmp-config.h>
#include <net-snmp/net-snmp-includes.h>
#include <net-snmp/agent/net-snmp-agent-includes.h>
#include <net-snmp/library/snmp_api.h>

#include "dadi.h"

#include <string.h>

#define DADI_PID_FILE "/tmp/dadi.pid"
#define DADI_USOCK_FILE "/tmp/dadi.usock"

static dadi_callback_t ActivationCallback;
extern netsnmp_session *agentx_callback_sess;
/* array of all the BB's known to DADI */
static S_BB_T **bbtab;

struct dadi_msg_event {
	int dadi_var;
	enum dadi_event_type event_type;
};

static struct dadi_allocator _glibc_allocator = {
	.init = NULL,
	.malloc = malloc,
	.calloc = calloc,
	.realloc = realloc,
	.dup = strdup,
	.free = free,
	.fini = NULL,
};

static const char *event2char (enum dadi_event_type event_type)
{
	static const char *event_strings[] = {
		"GET",
		"SET",
	};

	int eventt = (int) event_type;

	if ((eventt > DADI_EVENT_TYPE_SET) || (eventt < DADI_EVENT_TYPE_GET))
		return "???";
	return event_strings[eventt];
};

static volatile int running = 0;
int is_master;
/* Protect dadi initialization against a race with dadi_stop
   This protects mib_specific and accesses inside libnetsnmp */
static pthread_mutex_t init_lock = PTHREAD_MUTEX_INITIALIZER;

static int try_to_be_master(void);

static struct mib_specific *mib_specific = NULL;

/* these defines are missing on FC4 */
#ifndef LIST_HEAD_INITIALIZER
#define	LIST_HEAD_INITIALIZER(head)					\
	{ NULL }
#endif

#ifndef LIST_FOREACH
#define	LIST_FOREACH(var, head, field)					\
	for ((var) = ((head)->lh_first);				\
	     (var);							\
	     (var) = ((var)->field.le_next))
#endif

#ifndef LIST_NEXT
#define	LIST_NEXT(elm, field)		((elm)->field.le_next)
#endif

struct client {
	int fd;
	int answer_received;
	LIST_ENTRY(client) entries;
};

static LIST_HEAD(clients, client) clients =
LIST_HEAD_INITIALIZER(clients);

/* Array indicating whenever a given field has beed accessed or not */
static int *has_been_accessed;

static void default_dadi_log(int priority , const char *file, int line,
			     const char *function , const char *fmt, ...)
{
	char log_string[1024];
	static const char * priority_str[] =
		{ "EMERG  ",
		  "ALERT  ",
		  "CRIT	  ",
		  "ERR	  ",
		  "WARNING",
		  "NOTICE ",
		  "INFO   ",
		  "DEBUG  ",};

	struct timeval now;

	gettimeofday(&now, NULL);

	priority = ( (priority > LOG_DEBUG) || (priority < LOG_EMERG) ) ?
		LOG_DEBUG : priority ;

	va_list ap;

	va_start(ap, fmt);
	vsnprintf(log_string, sizeof(log_string), fmt, ap);
	fprintf(stderr, "%s %04ld:%03ld [%d] %s:%d (%s) %s\n",
		priority_str[priority],
		now.tv_sec % 1024,
		now.tv_usec / 1000,
		getpid(), file, line, function,
		log_string);
	va_end(ap);
}

static dadi_log_t dadi_log = default_dadi_log;

dadi_log_t dadi_set_loging_method(dadi_log_t new_dadi_log)
{
	dadi_log_t prev_dadi_log = dadi_log;
	dadi_log = new_dadi_log;

	return prev_dadi_log;
}

#define EMERG(fmt,pa...)						\
do {									\
	dadi_log(LOG_EMERG, __FILE__, __LINE__, __func__, fmt, ## pa);	\
} while (0)

#define ERROR(fmt,pa...)						\
do {									\
	dadi_log(LOG_ERR, __FILE__, __LINE__, __func__, fmt, ## pa);	\
} while (0)

#define INFO(fmt,pa...)							\
do {									\
	dadi_log(LOG_INFO, __FILE__, __LINE__, __func__, fmt, ## pa);	\
} while (0)

#define DADI_DEBUG(fmt,pa...)						\
do {									\
	dadi_log(LOG_DEBUG, __FILE__, __LINE__, __func__, fmt, ## pa);	\
} while (0)

static void dadi_master_add_client(int fd)
{
	struct client *client;
	client = calloc(1, sizeof(struct client));
	if (!client) {
		ERROR("Dadi: Not enough mem, cannot add new client");
		return;
	}

	client->fd = fd;
	LIST_INSERT_HEAD(&clients, client, entries);
}


/* TODO : should be related to snmp timeout */
static const struct timeval max_clients_timeout =
{ .tv_sec  = 2, .tv_usec = 0, };
static const struct timeval min_clients_timeout =
{ .tv_sec  = 0, .tv_usec = 10000, };

static struct timeval clients_timeout =
{ .tv_sec  = 1, .tv_usec = 0, };

void dadi_set_clients_timeout(const struct timeval *new_clients_timeout)
{
	clients_timeout =
		timercmp(new_clients_timeout, &max_clients_timeout, > ) ?
		max_clients_timeout :
		timercmp(new_clients_timeout, &min_clients_timeout, < ) ?
		min_clients_timeout :
		*new_clients_timeout ;
}

int dadi_evt(int dadi_var, enum dadi_event_type event_type)
{
	/* We're the master, send msgs to all the connected clients */
	if (is_master) {
		struct dadi_msg_event msg_evt = {
			.dadi_var = dadi_var,
			.event_type = event_type
		};

		struct client *client;
		struct client fake_client; /* used for safe client removal */

		int n_answer_waited = 0;
		LIST_FOREACH(client, &clients, entries) {
retry_send:
			DADI_DEBUG("master: sending event to fd %d var %d event_type %s(%d)",
			      client->fd, dadi_var, event2char(event_type),
			      event_type);
			if (send(client->fd, &msg_evt,
				 sizeof(msg_evt), MSG_NOSIGNAL) <= 0) {
				switch (errno) {
				case EPIPE:
				case ECONNRESET:
					LIST_NEXT(&fake_client, entries) =
						LIST_NEXT(client, entries);
					LIST_REMOVE(client, entries);
					close(client->fd);
					free(client);
					client = &fake_client;
					break;
				case EINTR:
					goto retry_send;
					break;
				default :
					EMERG("send: %s", strerror(errno));
					continue;
				}
			}
			client->answer_received = 0;
			n_answer_waited++;
		}

		struct timeval timeout = clients_timeout;

		while (n_answer_waited) {
			fd_set rfds;
			int nfds;
		retry_select:
			FD_ZERO(&rfds);
			nfds = 0;

			LIST_FOREACH(client, &clients, entries) {
				if (!client->answer_received) {
					FD_SET(client->fd, &rfds);
					if (nfds < client->fd + 1)
						nfds = client->fd + 1;
				}
			}

			int res = select(nfds, &rfds, NULL, NULL,
					 &timeout);

			if (res < 0) {
				switch (errno) {
				case EINTR:
					goto retry_select;
				default :
					EMERG("select: %s", strerror(errno));
					continue;
				}
			} else if (res == 0) { /* timeout */
				LIST_FOREACH(client, &clients, entries) {
					if (!client->answer_received) {
						LIST_NEXT(&fake_client,
							  entries) =
							LIST_NEXT(client,
								  entries);
						LIST_REMOVE(client, entries);
						close(client->fd);
						free(client);
						client = &fake_client;
					}
				}
				n_answer_waited = 0;
				continue;
			}
			LIST_FOREACH(client, &clients, entries) {
				if (!FD_ISSET(client->fd, &rfds))
					continue;
				int dummy;
				client->answer_received = 1;
				DADI_DEBUG("master: received answer fd %d var %d event_type %s(%d)",
				      client->fd, dadi_var, event2char(event_type),
				      event_type);

				if (sizeof(dummy) !=
				    recv(client->fd, &dummy,
					 sizeof(dummy), 0)) {
					INFO("recv: %s", strerror(errno));
					LIST_NEXT(&fake_client,
						  entries) =
						LIST_NEXT(client,
							  entries);
					LIST_REMOVE(client, entries);
					close(client->fd);
					free(client);
					client = &fake_client;
				}
				n_answer_waited --;
			}
		}
	}
	if (ActivationCallback != NULL) {
		return ActivationCallback(dadi_var, event_type);
	} else {
		return -1;
	}
}

/* must be called with init_lock held */
static int startup_agent(const char *name)
{
	if (!mib_specific)
		return -1;

	/* we are a subagent */
	netsnmp_ds_set_boolean(NETSNMP_DS_APPLICATION_ID,
			       NETSNMP_DS_AGENT_ROLE, 1);
	netsnmp_register_loghandler(NETSNMP_LOGHANDLER_STDERR, LOG_ERR);

	/* initialize tcpip, if necessary */
	SOCK_STARTUP;
	/* initialize the agent library */
	init_agent(name);
	if (__dadi_allocator->init)
		__dadi_allocator->init();
	/* register snmp callbacks */
	mib_specific->register_snmp();
	/* startup the agent */
	init_snmp(name);

	return 0;
}


static void *usock_server_thread(void *arg __attribute__ ((unused)))
{
	int listen_socket;
	struct sockaddr_un local;
	int len;

	listen_socket = socket(AF_UNIX, SOCK_STREAM, 0);
	if (listen_socket == -1) {
		EMERG("socket: %s", strerror(errno));
		return NULL;
	}
	local.sun_family = AF_UNIX;
	strcpy(local.sun_path, DADI_USOCK_FILE);
	unlink(local.sun_path);
	len = strlen(local.sun_path) + sizeof(local.sun_family);
	if (bind(listen_socket, (struct sockaddr *)&local, len) == -1) {
		EMERG("bind: %s", strerror(errno));
		return NULL;
	}
	if (listen(listen_socket, 5)) {
		EMERG("listen: %s", strerror(errno));
		return NULL;
	}

	while (running) {
		int client_socket, ret, resp;
		char client_sha1[2048];

		client_socket = accept(listen_socket, NULL, NULL);
		if (client_socket == -1) {
			EMERG("accept: %s", strerror(errno));
			break;
		}

		/*
		 * Check if client's version matches ours. If not, drop it
		 */
		ret = read(client_socket, client_sha1, sizeof(client_sha1));
		if (ret <= 0) {
			close(client_socket);
			continue;
		}

		if (strncmp(mib_specific->datatree_sha1(),
			    client_sha1,
			    strlen(mib_specific->datatree_sha1()))) {
			ERROR("Dadi: attempted connection from "
			      "incompatible client, ignoring");
			resp = 0;
			ret = write(client_socket, &resp, sizeof(resp));
			if (ret < 0) {
				ERROR("Dadi: failed to write error back, ignoring");
			}
			close(client_socket);
			continue;
		}

		resp = 1;
		ret = write(client_socket, &resp, sizeof(resp));
		if (ret != sizeof(resp)) {
			close(client_socket);
			continue;
		}

		/* Sanity checks OK, add client */
		dadi_master_add_client(client_socket);
	}

	close(listen_socket);
	return NULL;
}

static void dadi_client(void)
{
	int client_socket;
	struct sockaddr_un remote;
	int len, ret;
	static int retries = 0;

	client_socket = socket(AF_UNIX, SOCK_STREAM, 0);
	if (client_socket == -1) {
		EMERG("socket: %s", strerror(errno));
		return;
	}
	remote.sun_family = AF_UNIX;
	strcpy(remote.sun_path, DADI_USOCK_FILE);
	len = strlen(remote.sun_path) + sizeof(remote.sun_family);
retry_connect:
	if (connect(client_socket, (struct sockaddr *)&remote, len) == -1) {
		switch (errno) {
		case ECONNREFUSED:
		case ENOENT:
		case ETIMEDOUT:
			retries++;
			close(client_socket);
			if (retries > 50) {
				EMERG("Cannot open dadi socket (more than 50):"
				      " %s", strerror(errno));
				return;
			}
			/* we will retry later --- so wait a little */
			usleep(100000);
			return;
		case EINTR:
			goto retry_connect;
			break;
		default:
			EMERG("connect): %s", strerror(errno));
			return;
		}
	}
	/* Send our expected version to the server */
	len = write(client_socket, mib_specific->datatree_sha1(),
		    strlen(mib_specific->datatree_sha1())+1);
	if (len <= 0) {
		EMERG("client handshake write: %s", strerror(errno));
		return;
	}
	/* Read back if server accepted us or not */
	len = read(client_socket, &ret, sizeof(ret));
	if (len <= 0) {
		EMERG("client handshake read: %s", strerror(errno));
		return;
	}
	if (!ret) {
		EMERG("Dadi: server version incompatible, aborting");
		return;
	}

	retries = 0;

	/* Process dadi events are they are received through the socket */
	while (running) {
		struct dadi_msg_event msg_event;
		int n;
		fd_set rd_set;
		struct timeval tv = { .tv_sec = 1, .tv_usec = 0 };

		FD_ZERO(&rd_set);
		FD_SET(client_socket, &rd_set);

		ret = select(client_socket+1, &rd_set, NULL, NULL, &tv);
		if (ret == 0 || (ret < 0 && errno == EINTR)) {
			continue;
		}

		n = recv(client_socket, &msg_event, sizeof(msg_event), 0);
		if (n == 0) {
			close(client_socket);
			return;
		}
		if (n < 0) {
			if (errno == EINTR) {
				continue;
			}
			EMERG("recv: %s", strerror(errno));
			close(client_socket);
			return;
		}

		DADI_DEBUG("client: received event var %d event_type %s(%d)",
		      msg_event.dadi_var, event2char (msg_event.event_type),
		      msg_event.event_type);

		dadi_evt(msg_event.dadi_var, msg_event.event_type);

		int dummy = 0;
	retry_send:
		n = send(client_socket, &dummy, sizeof(dummy), MSG_NOSIGNAL);
		if (n == 0) {
			close(client_socket);
			return;
		}
		if (n < 0) {
			if (errno == EINTR) {
				goto retry_send;
			}
			EMERG("send: %s (dadi_var %d, event_type %d)",
			      strerror(errno),
			      msg_event.dadi_var, msg_event.event_type);
			close(client_socket);
			return;
		}
	}

	close(client_socket);
	return;
}

static void sigpipe_handler(int sig, siginfo_t *info, void *arg);

static void sigpipe_handler(int sig __attribute__ ((unused)),
			    siginfo_t *info __attribute__ ((unused)),
			    void *arg __attribute__ ((unused)))
{
	EMERG("Dadi: received a SIGPIPE, exiting");
	/*
	 * Kill ourselves, to my knowledge there's nothing we can do.
	 * As of net-snmp 5.4 a timeouted agent just gets dumped by the
	 * net-snmpd
	 */
	return;
}

void dadi_set_mibdir(const char *path)
{
	netsnmp_get_mib_directory();
	netsnmp_set_mib_directory(path);
}

void dadi_set_cfgdir(const char *path)
{
	char buf[1024];
	const char *opath;
	opath = get_configuration_directory();
	snprintf(buf, sizeof(buf), "%s:%s", opath, path);
	set_configuration_directory(buf);
	opath = get_configuration_directory();
}


/** @ingroup dadi_itf
 * @brief first initialisation function for the library.
 *
 * This function makes the pre-initialisation of the library. It allocates
 * necessary strutures and so on.
 * @param theActivationCallback the activation callback that will be called
 * when parameters are modified
 * @return 0 if the initialisation fails, or a pointer on the global
 * configuration and counters
 */
void *dadi_first_init(dadi_callback_t theActivationCallback)
{
	void *ret = NULL;

	pthread_mutex_lock(&init_lock);
	/* Call libmib.so to regiter the mib_specific global */
	register_mib_init();
	if (mib_specific == NULL) {
		ERROR("Dadi not started... no mib handled");
		goto out;
	}
	struct sigaction sigpipe_action = { .sa_sigaction = sigpipe_handler, };

	sigaction(SIGPIPE, &sigpipe_action, NULL);

	/* Set the callback */
	if (theActivationCallback == NULL)
		goto out;
	ActivationCallback = theActivationCallback;

	/* build the blackboard and allocate the structure */
	bbtab = mib_specific->register_bb();

out:
	pthread_mutex_unlock(&init_lock);
	__dadi_allocator = &_glibc_allocator;
	return ret;
}

static int agent_check_and_process_sig(void)
{
    int             numfds;
    fd_set          fdset;
    struct timeval  timeout = { 0, 50000 }, tv;
    int             count;
    int 			fakeblock;
    static int		netsnmp_store_needed = 1;

    numfds = 0;
    FD_ZERO(&fdset);
    snmp_select_info(&numfds, &fdset, &tv, &fakeblock);
    count = select(numfds, &fdset, NULL, NULL, &timeout);

    if (count > 0) {
        snmp_read(&fdset);
    } else {
        switch (count) {
        case 0:
            snmp_timeout();
            break;
        case -1:
            if (errno != EINTR) {
                snmp_log_perror("select");
            }
            return -1;
        default:
            return -1;
        }
    }
    if (netsnmp_store_needed) {
    	snmp_store(netsnmp_ds_get_string(NETSNMP_DS_LIBRARY_ID,
    			NETSNMP_DS_LIB_APPTYPE));
    	netsnmp_store_needed = 0;
    }
    run_alarms();
    netsnmp_check_outstanding_agent_requests();
    return count;
}

/** main dadi loop. Never ends unless ef dadi_stop is called
 * @return 0 if the library closed successfully, -1 otherwise
 */
int dadi_mainloop(int start_usock_thread)
{
	int ret;

	is_master = try_to_be_master();

	pthread_mutex_lock(&init_lock);
	if (is_master) {
		ret = startup_agent("snmpd");
		if (ret < 0) {
			ERROR("Dadi not started... no mib handled");
			pthread_mutex_unlock(&init_lock);
			return -1;
		}
	}

	running = 1;
	INFO("Dadi: started");

	pthread_mutex_unlock(&init_lock);

	while (running) {
		if (is_master) {
			if (start_usock_thread) {
				pthread_t th;
				pthread_create(&th, NULL, &usock_server_thread, NULL);
			}

			while (running) {
				if (agent_check_and_process_sig() < 0) {
					if (errno == EINTR)
						sched_yield();
				}
			}
		} else {
			dadi_client();
			if (running) {
				is_master = try_to_be_master();
				if (is_master) {
					pthread_mutex_lock(&init_lock);
					ret = startup_agent("snmpd");
					if (ret < 0) {
						ERROR("Failed to start agent, no mib handled");
						/* Don't exit */
					}
					pthread_mutex_unlock(&init_lock);
				}
			}
		}
	}
	running = 0;
	return 0;
}

/**
 * @ingroup dadi_itf
 * @brief function that do what's is needed to stop the main_loop
 *
 * @return 0 if the main loop successfully stops, -1 otherwise
 */
int dadi_terminate_request(void)
{
	running = 0;
	return 0;
}

static int dadi_wipe_fds(void)
{
	fd_set s;
	int i = 0;
	int maxfd = 0;
	int fakeblock = 0;
	int opensocks = 0;
	struct timeval tv;

	/* preemptively close all libsnmp descriptors */
	FD_ZERO(&s);
	opensocks = snmp_select_info(&maxfd, &s, &tv, &fakeblock);
	for (i = maxfd - 1; i >= 0 && opensocks; --i) {
		if (FD_ISSET(i, &s)) {
			close(i);
			--opensocks;
		}
	}
	return i;
}

/**
 * @ingroup dadi_itf
 * @brief function that frees all allocated data within the dadi library
 *
 * @return 0 if the main loop successfully stops, -1 otherwise
 */
int dadi_stop(void)
{
	int i = 0;

	pthread_mutex_lock(&init_lock);
	/*
	 * Although this should be called, it actually double frees some stuff
	 * in net-snmp (shutdown_agent, and snmp_shutdown cannot be called
	 * together
	 */
	if (is_master) {
		SOCK_CLEANUP;
	}

	/* TSP blackboard cleanup */
	while (bbtab && bbtab[i] != NULL) {
		bb_detach(&bbtab[i]);
		i++;
	}
	mib_specific = NULL;
	agentx_callback_sess = NULL;
	dadi_wipe_fds();
	if (__dadi_allocator->fini)
		__dadi_allocator->fini();
	INFO("Dadi: stopped");
	snmp_shutdown(NULL);
	shutdown_agent();

	pthread_mutex_unlock(&init_lock);

	return 0;
}

/**
 *  test_callback: prints default message when called
 */
static int test_callback(int var, enum dadi_event_type event_type)
{
	switch (event_type) {
	case DADI_EVENT_TYPE_GET:
		has_been_accessed[var] |= 1;
		DADI_DEBUG("Get done : %d", var);
		break;
	case DADI_EVENT_TYPE_SET:
		has_been_accessed[var] |= 2;
		DADI_DEBUG("Set done : %d", var);
		break;
	default:
		ERROR("Bad event type : %d", event_type);
		break;
	}
	return 1;
}

static void print_accesses(void);
static void print_accesses(void)
{
	int i;
	for (i=0; i < mib_specific->max_id; i++) {
		DADI_DEBUG("COV %d: %d\n", i, has_been_accessed[i]);
	}
}

/**
 *  dadi_test: test entry point for the library
 */
int dadi_test(int argc, char **argv);
int dadi_test(int argc __attribute__ ((unused)),
	      char **argv __attribute__ ((unused)))
{
	typedef void (*sighandler_dadi_t)(int);

	has_been_accessed = calloc(1, sizeof(int) * mib_specific->max_id);
	dadi_callback_t theActivationCallback = test_callback;
	dadi_first_init(theActivationCallback);
	signal(SIGINT, (sighandler_dadi_t)dadi_terminate_request);
	dadi_mainloop(1);
	dadi_stop();

	print_accesses();

	exit(0);
	return 0;
}

static int try_to_be_master(void)
{
	int fd = -1;
	char *pidfile;
	char *tmpdir;
	char buf[256];

	tmpdir = getenv("TMPDIR");
	if (!tmpdir) {
		pidfile = strdupa(DADI_PID_FILE);
	} else {
		size_t blen;
		char *filename;
		filename = basename(DADI_PID_FILE);
		blen = strlen(tmpdir) + strlen(filename) + 2;
		pidfile = (char *)alloca(blen);
		if (pidfile) {
			snprintf(pidfile, blen, "%s/%s", tmpdir, filename);
		}
	}
	if (!pidfile) {
		return 0;
	}

	fd = open(pidfile, O_CREAT | O_EXCL | O_WRONLY,
		      S_IRUSR | S_IWUSR);
	if (fd == -1) {
		if (errno != EEXIST) {
			EMERG("open (create): %s", strerror(errno));
			return 0;
		}
		fd = open(pidfile, O_WRONLY);
		if (fd == -1) {
			EMERG("open (write): %s", strerror(errno));
			return 0;
		}
	}
	if (lockf(fd, F_TLOCK, 0) == -1) {
		if (errno == EAGAIN) {
			close(fd);
			return 0; /* there is already a client */
		}
		EMERG("lockf: %s", strerror(errno));
		return 0;
	}
	if (ftruncate(fd, 0) == -1) {
		EMERG("ftruncate: %s", strerror(errno));
		close(fd);
		return 0;
	}
	sprintf(buf, "%d", getpid());
	if (write(fd, buf, strlen(buf)) < 0) {
		EMERG("write (pid): %s", strerror(errno));
		close(fd);
		return 0;
	}
	return 1;
}

void dadi_register_mib(struct mib_specific *mib)
{
	if (mib_specific != NULL) {
		ERROR("Can only register one mib !!!\n");
		return;
	}
	mib_specific = mib;
}

