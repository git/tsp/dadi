# bb_dumper: dumps a blackboard and SNMP agent code
#
# Copyright (C) 2006,2007,2008
# Frederik Deweerdt <frederik.deweerdt@gmail.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program; if not, write to the Free Software Foundation,
# Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

require 'data_tree.rb'
require 'erb'
require 'yaml'
require 'digest/sha1'
require 'optparse'

class Bb_dumper
  attr_reader :efile_string, :hfile_string, :cfile
  @@bit_no = 0

  def Bb_dumper.add_options(optparser, options)
    optparser.on("-w", "--write-all", "Ignore readonly attributes on the MIB") { |v| options[:writeall] = v }
  end#add_options

  def initialize(options)
    @sha1 = ""
    @options = options
    @dest = options[:dest]
    @c_prefix = options[:c_prefix]
    @templates=YAML::load(File.open("#{ENV['DADI_HOME']}/tmpl/bb.yaml"))
    @daditype_mapper = {
      "int" => "DADI_INT",
      "uint" => "DADI_UINT",
      "uint64_t" => "DADI_UINT64",
      "counter32" => "DADI_COUNTER",
      "chartab" => "DADI_CHARTAB",
      "macaddr" => "DADI_MACADDR",
      "ipaddr" => "DADI_IPADDR",
    }

    # Support read-only or not, depending on --write-all
    if options[:writeall] then
      @dadiaccess_mapper = {
      "readwrite" => "DADI_RW",
      "readonly" => "DADI_RW"
      }
    else
      @dadiaccess_mapper = {
      "readwrite" => "DADI_RW",
      "readonly" => "DADI_RO"
      }
    end#if
    @bbtype_mapper = {
          "double" => "E_BB_DOUBLE",
          "float" => "E_BB_FLOAT",
          "int8_t" => "E_BB_INT8",
          "int16_t" => "E_BB_INT16",
          "int32_t" => "E_BB_INT32",
          "int" => "E_BB_INT32",
          "uint" => "E_BB_UINT32",
          "int64_t" => "E_BB_INT64",
          "uint8_t" => "E_BB_UINT8",
          "uint16_t" => "E_BB_UINT16",
          "uint32_t" => "E_BB_UINT32",
          "uint64_t" => "E_BB_UINT64",
          "char" => "E_BB_CHAR",
          "unsigned char" => "E_BB_UCHAR"
    }
    @bbtype_mapper.default = "E_BB_USER";

  end#initialize

  def dest_folders
    return ["#{@dest}/src", "#{@dest}/include"]
  end#dest_folders

  def prolog
    @cfile = File.new("#{@dest}/src/#{@c_prefix}mib.c", "w+")
    @hfile = File.new("#{@dest}/include/#{@c_prefix}inlines_mib.h", "w+")
    @efile = File.new("#{@dest}/include/#{@c_prefix}enums.h", "w+")

    @snmp_mibs_to_register = ""
    @bb_hack = ""
    @hfile.puts "#ifndef __MIB_H__\n#define __MIB_H__"
    @hfile.puts "#include \"#{@c_prefix}enums.h\""
    @efile.puts "#ifndef __ENUMS_H__\n#define __ENUMS_H__"
  end

  def get_template(name, default=nil)
    begin
      template = ERB.new(@templates[name])
      return template.result(@tmpl_data.to_binding)
    rescue
      raise "Cant find template for #{name}: #{$!}" if default == nil
      return default
    end
  end#get_template

  def dump_types (tree,bbname)
    # Generate the list of the variables that can trigger
    # events on accessors
    tree.nodes.each do |n|
      @tmpl_data = n
      @tmpl_data["bit_no"] = @@bit_no
      @efile_string += get_template("define_decl")
      @efile_string += get_template("define_oid")
      @@bit_no += 1
    end#node.each

    tree.tables.each do |n|
      @tmpl_data = n
      @tmpl_data["bit_no"] = @@bit_no
      @efile_string += get_template("define_decl")
      @@bit_no += (n["length"].to_i * n["columns"].size)
    end#node.each

    @tmpl_data = Node.new
    @tmpl_data["name"] = "#{bbname}_max"
    @tmpl_data["bit_no"] = @@bit_no
    @efile_string += get_template("define_decl")

    tree.types.each_key do
      |text_convention|

      next if tree.types[text_convention] == "chartab"
      @tmpl_data = Node.new
      @tmpl_data["ctype"] = tree.types[text_convention]
      @tmpl_data["typedef_name"] = text_convention
      @efile_string += get_template("typedef_decl")
    end#types.each
    #Generate the enums defined in the MIB
    tree.enums.each do
      |e|
      enum_pairs = ""
      e["names"].each { |v|
        @tmpl_data = Node.new
        @tmpl_data["name"] = "#{e['name']}_#{v}"
        @tmpl_data["value"] = e["values"][v]
        enum_pairs += get_template("enum_pair")
      }
      @tmpl_data = e
      @tmpl_data["enum_pairs"] = enum_pairs
      @efile_string += get_template("enum_decl")
    end#enums.each

  end#dump_types

  def dump_datatree(tree,bbname)
    @sha1 += Digest::SHA1.hexdigest(tree.to_s)
    @efile_string = ""
    @cfile_string = ""
    @hfile_string = ""
    dump_types(tree,bbname)
    publishes = ""
    snmp_register_calls = ""
    getters_decl = ""
    setters_decl = ""
    getters = ""
    setters = ""

    #Add additional information on nodes and tables
    tree.nodes.each { |n|
      n["c_oid"] = n["oid"].gsub(".", ",")+",0"
      n["bbname"] = bbname
      n["dadi_type"] = @daditype_mapper[n['type'].to_s]
      n["dadi_access"] = @dadiaccess_mapper[n['access'].to_s]
      @tmpl_data = n
      n["decl"] = get_template("#{n['type']}_decl")
    }
    tree.tables.each { |t|
      t["c_oid"] = t["oid"].gsub(".", ",")
      t["min"] = 0
      t["max"] = t["length"].to_i - 1
      t["columns"].each { |n|
        n["bbname"] = bbname
        n["dadi_type"] = @daditype_mapper[n['type'].to_s]
        n["dadi_access"] = @dadiaccess_mapper[n['access'].to_s]
        @tmpl_data = n
        n["decl"] = get_template("#{n['type']}_decl")
      }
    }

    @tmpl_data = Node.new
    @tmpl_data["bbname"] = bbname
    @tmpl_data["nodes"] = tree.nodes
    @hfile_string += get_template("extern_mib_pointer")

    @tmpl_data["c_prefix"] = @c_prefix
    @cfile_string += get_template("mib_c_includes")
    @cfile_string += get_template("mib_c_header")
    @cfile_string += get_template("mib_pointer_decl")

    tree.tables.each do
      |t|
      @tmpl_data = t
      @tmpl_data["bbname"] = bbname
      @tmpl_data["table_name"] = t["name"]

      @hfile_string += get_template("table_structure")
      @hfile_string += get_template("table_length")

      publishes += get_template("table_alias_publish")

      @hfile_string += get_template("table_min_getter_decl")
      @hfile_string += get_template("table_max_getter_decl")

      @tmpl_data["size"] = t["columns"].size
      @tmpl_data["min"] = 0
      @tmpl_data["max"] = t["length"].to_i - 1

      @hfile_string += get_template("table_tester_decl")
      @hfile_string += get_template("table_range")
      @hfile_string += get_template("table_get_index")
      @hfile_string += get_template("table_get_offset")

      col_no = 0
      t["columns"].each do
        |c|
        @tmpl_data = c
        @tmpl_data["bbname"] = bbname
        @tmpl_data["col_no"] = col_no
        @tmpl_data["size"] = t["columns"].size
        @tmpl_data["table_name"] = t["name"]
        @tmpl_data["bb_type"] = @bbtype_mapper[c['type'].to_s]

        @hfile_string += get_template("row_id_getter_decl")
        @hfile_string += get_template("row_offset_decl")

        getters_decl += get_template("table_getter_#{c['type']}_decl")
        getters_decl += get_template("table_getter_#{c['type']}_pointer_decl", "")
        setters_decl += get_template("table_setter_#{c['type']}_decl")

        getters += get_template("table_getter_#{c['type']}")
        getters += get_template("table_getter_#{c['type']}_pointer", "")
        setters += get_template("table_setter_#{c['type']}")
        publishes += get_template("table_member_alias_publish")

        col_no += 1
      end#each.columns
    end#each

    @tmpl_data = Node.new
    @tmpl_data["bbname"] = bbname
    @tmpl_data["nodes"] = tree.nodes if tree.nodes.size > 0
    @tmpl_data["tables"] = tree.tables if tree.tables.size > 0

    @hfile_string += get_template("main_structure")

    tree.nodes.each do
      |n|
      @tmpl_data = n
      @tmpl_data["bbname"] = bbname
      @tmpl_data["bb_type"] = @bbtype_mapper[n['type'].to_s]

      getters_decl += get_template("getter_#{n['type']}_decl")
      getters_decl += get_template("getter_#{n['type']}_pointer_decl", "")
      setters_decl += get_template("setter_#{n['type']}_decl")

      getters += get_template("getter_#{n['type']}")
      getters += get_template("getter_#{n['type']}_pointer", "")
      setters += get_template("setter_#{n['type']}")
      publishes += get_template("alias_publish")

    end#each

    @cfile_string += setters
    @cfile_string += getters

    @hfile_string += setters_decl
    @hfile_string += getters_decl

    @tmpl_data = Node.new
    @tmpl_data["bbname"] = bbname
    @tmpl_data["nodes"] = tree.nodes if tree.nodes.size > 0
    @tmpl_data["tables"] = tree.tables if tree.tables.size > 0

    nb_elem = tree.nodes.size + 1
    tree.tables.each { |t| nb_elem += t["columns"].size }
    nb_elem += tree.tables.size
    @tmpl_data["nb_elem"] = nb_elem

    @tmpl_data["alias_publish_calls"] = publishes

    @cfile_string += get_template("register_bb")

    @cfile_string += get_template("register_snmp")

    @tmpl_data = Node.new
    @tmpl_data["bbname"] = bbname
    @tmpl_data["nodes"] = []
    tree.nodes.each { |n|
      if n["type"] == "int" then
        @tmpl_data["nodes"] << n
      end#if
    }

    tree.notifications.each do
      |n|
      @tmpl_data = n
      @tmpl_data["c_oid"] = n["oid"].gsub(".", ",")+",0"
      #
      # Get the objects we need to notify about
      #
      n["objects"].each do
        |nobj|
        tree.nodes.each do
          |tree_node|

          if tree_node["name"] == nobj["name"]
          then
            nobj["bbname"] = bbname
            nobj["type"]  = @daditype_mapper[tree_node['type'].to_s]
            nobj["c_oid"] = tree_node["oid"].gsub(".", ",")+",0"
          end#if
        end#each.node
      end#each.objects
      @hfile_string += get_template("trap_func_decl")
      @cfile_string += get_template("trap_func")
    end#notifications.each

    @cfile_string += get_template("mib_c_footer")

    @hfile.puts @hfile_string
    @cfile.puts @cfile_string
    @efile.puts @efile_string
    @snmp_mibs_to_register+="#{bbname}_init_snmp();\n"

    @bb_hack+="#{bbname} = #{bbname}_register_bb();\n"
    @bb_hack+="{ extern S_BB_T *bb_#{bbname}; bbtab[bbtab_index++] = bb_#{bbname}; }\n"

  end#dump_datatree

  def epilog
    #
    # This part allows the generated lib to be used in a standalone fashion
    #
    @cfile.puts "const char dadi_interp[] __attribute__((section(\".interp\"))) = \"/lib/ld-linux.so.2\";"
    @cfile.puts "int standalone_start(void);"
    @cfile.puts <<EOS
    void register_mib_init(void);
    int standalone_start()
    {
      extern int dadi_test(void);
      /* _init is not called when launched as a standalone
       * program */
      register_mib_init();
      return dadi_test();
    }
EOS

    #
    # Register the various mibs handled by this lib
    #
    @cfile.puts "static int register_snmp(void);"
    @cfile.puts "static int register_snmp() { #{@snmp_mibs_to_register} return 1; }"

    @hfile.puts "const char *datatree_sha1(void);"
    @cfile.puts "const char *datatree_sha1() { static const char sha1[] = \"#{@sha1}\"; return sha1; }"

    @cfile.puts <<EOS
      static S_BB_T **register_bb(void);
      static S_BB_T **register_bb() {
        S_BB_T **bbtab = calloc(MAX_BBS, sizeof (*bbtab));
        if (!bbtab) return NULL ;
        int bbtab_index = 0;
        #{@bb_hack}
        return bbtab;
      }

	void register_mib_init(void)
	{
		static struct mib_specific this_mib_specific = {
			.max_id = #{@@bit_no},
			.register_bb = register_bb,
			.register_snmp = register_snmp,
			.datatree_sha1 = datatree_sha1,
		};

		dadi_register_mib(&this_mib_specific);
		return ;
	}
EOS


    @hfile.puts "#endif /* __MIB_H__ */"
    @efile.puts "#endif /* __ENUMS_H__ */"

    @cfile.close
    @efile.close
    @hfile.close
  end#epilog


end#class Bb_dumper
