# tex_dumper: dumps a tex file documenting the parsed input
# Copyright (C) 2008  Frederik Deweerdt <frederik.deweerdt@gmail.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program; if not, write to the Free Software Foundation,
# Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

require 'data_tree.rb'
require 'celsoft.com/template'
require 'yaml'

class Tex_dumper
  def description(e, match)
    e["description"]=~/MDD_APP PRM #{match}=([^!]*)%!MDD_APP/
    return $1
  end

  def build_desc(string)
    string.gsub!(/%MDD_APP(.*)%!MDD_APP/,"")
    string.gsub!(/_/,"\\_")
    string="\\begin{tabular}{p{8cm}}#{string}\\end{tabular}"
    return string

  end

  def min(e)
    return " " if e["range"] == nil
    return e["range"].min
  end

  def max(e)
    return " " if e["range"] == nil
    return e["range"].max
  end

  def init_display
    @display = [
    # in-dumper name              filter to retrieve the value from the node         Tex name          Tex col alignment
    {:hashname => "name"        ,:filter => nil                                     ,:name => "Param name"   ,:align => "l"},
    {:hashname => "units"       ,:filter => nil                                     ,:name => "Unit"         ,:align => "c"},
    {:hashname => "persistence" ,:filter => proc{ |e| description(e,"persistence")} ,:name => "Persistence"  ,:align => "c"},
    {:hashname => "minvalue"    ,:filter => proc{ |e| min(e)}                       ,:name => "Min value"    ,:align => "c"},
    {:hashname => "default"     ,:filter => nil                                     ,:name => "Default value",:align => "c"},
    {:hashname => "maxvalue"    ,:filter => proc{ |e| max(e)}                       ,:name => "Max value"    ,:align => "c"},
    {:hashname => "applicable"  ,:filter => proc{ |e| description(e,"applicable")}  ,:name => "Applicable"   ,:align => "c"},
    {:hashname => "access"      ,:filter => nil                                     ,:name => "Modifiable"   ,:align => "c"},
    {:hashname => "commismode"  ,:filter => proc{ |e| description(e,"commode")}     ,:name => "CM"           ,:align => "c"},
    {:hashname => "porte"       ,:filter => proc{ |e| description(e,"porte")}       ,:name => "Porte"        ,:align => "c"},
    {:hashname => "profil"      ,:filter => proc{ |e| description(e,"cprofil")}     ,:name => "Profile"      ,:align => "c"},
    {:hashname => "userfriendly",:filter => proc{ |e| description(e,"userfriendly")},:name => "Name"         ,:align => "l"},
    {:hashname => "description",:filter => proc{ |e| build_desc(e["description"])}  ,:name => "Description"  ,:align => "l"},
    ]
  end#init_display


  def initialize(opts)
    @outfile_name = "#{File.basename(opts[:xml_file][0], ".xml")}.tex"
    @outfile = File.new(@outfile_name, "w+")
  end#initialize

  def dump_datatree(tree, name)
    @tree = tree
    init_display

    @tree.nodes.each do
      |e|
      if e["status"].downcase=="current" then
        @display.each do
          |key|
          if key[:filter] != nil then
            e["description"] =~ key[:filter]
            e[key[:hashname]] = key[:filter].call(e)
          end
        end#each
      end#if
    end#each

    @tree.tables.each do
      |t|
      t["columns"].each do
        |e|
        if e["status"].downcase=="current" then
          @display.each do
            |key|
            if key[:filter] != nil then
              e["description"] =~ key[:filter]
              e[key[:hashname]] = key[:filter].call(e)
            end
          end#each
        end
      end
    end

    @summary_dump += dumptable(["userfriendly" ,"name", "description"],0)
    @detailed_dump += dumptable(["name","units","persistence","minvalue",
                                 "default","maxvalue","applicable","access",
                                 "commismode","porte","profil"],60)
  end#dump_datatree


  def get_relative_hash(x)
    @display.each do |h|
      return h if h[:hashname] == x
    end#each
  end#get_relative_hash


  def dump_tex_tableheader(cols, angle)
    string = ""
    string += "\\begin{longtable}{"
    cols.each do |col|
      string += "|#{col[:align]}"
    end

    string += "|}\\hline\n"

    first_column = true
    cols.each do |col|
      string += "&" unless first_column
      first_column = false
      string += "{\\rotatebox{#{angle}}{\\bf{#{col[:name]}}}}"
    end

    string += "\\\\ \\hline \\endfirsthead \\hline\n"

    first_column = true
    cols.each do |col|
      string += "&" unless first_column
      first_column = false
      string += "{\\rotatebox{#{angle}}{\\bf{#{col[:name]}}}}"
    end

    string += "\\\\ \\hline \\endhead \\hline\n"
    return string
  end#dump_tex_tableheader

  def dump_tex_tablefooter()
    string = ""
    string += "\\\\ \\hline\n"
    string += "\n\\end{longtable}\n"
    return string
  end#dump_tex_tablefooter

  def dumptable(cols, angle)
    string = ""
    #col sous le format ["col1", "col2", ...,"coln"]
    cols.collect!{ |x| get_relative_hash(x) }

    #
    # Dump the nodes
    #
    string += dump_tex_tableheader(cols, angle)

    first_node = true
    @tree.nodes.each do
      |node|
      if node["status"].downcase == "current" then
        string += "\\\\ \\hline\n" unless first_node
        first_node = false

        first_col = true
        cols.each do |col|

          string += "&" unless first_col
          first_col = false

          if node[col[:hashname]] == nil || node[col[:hashname]].to_s.empty?
            string += " "
          else
            string += node[col[:hashname]].to_s
          end

        end#each
      end#if
    end#each

    string += dump_tex_tablefooter()

    #
    # Dump the tables
    #
    @tree.tables.each do |t|
      string += "Description of table #{t['name']}. Its size is #{t["length"]} elements at maximum"
      string += dump_tex_tableheader(cols, angle)

      first_row = true
      t["columns"].each do |e|
        if e["status"].downcase=="current" then

          string += "\\\\ \\hline\n" unless first_row

          if not first_row then

            first_column = true
            cols.each do |col|

              string += "&" unless first_column
              first_column = false

              if e[col[:hashname]] == nil || e[col[:hashname]].to_s.empty?
                string += " "
              else
                string += e[col[:hashname]].to_s
              end
            end#each
          end#if
        end #if
        first_row = false
      end#each
      string += dump_tex_tablefooter()
    end#each
    return string
  end#dumptable


  def prolog
    @summary_dump=""
    @detailed_dump=""
  end#prolog

  def epilog
    @outfile.printf(build_tex_header())


    @outfile.printf(@summary_dump)
    @outfile.printf("\\newpage\n")
    @outfile.printf(@detailed_dump)

    @outfile.printf(build_tex_footer())
    @outfile.close()

    Kernel.system("pdflatex #{@outfile_name} -interaction=nonstopmode > /dev/null")
    Kernel.system("pdflatex #{@outfile_name} -interaction=nonstopmode > /dev/null")
    Kernel.system("pdflatex #{@outfile_name} -interaction=nonstopmode > /dev/null")
  end#epilog

  def build_tex_header()
    return <<EOS
    \\documentclass[a4paper]{book}
    \\usepackage{a4wide}
    \\usepackage{makeidx}
    \\usepackage{fancyhdr}
    \\usepackage{lastpage}
    \\usepackage{graphicx}
    \\usepackage{multicol}
    \\usepackage{longtable}
    \\usepackage{lscape}
    \\usepackage[landscape,pdftex]{geometry}
    \\usepackage{textcomp}
    \\usepackage{alltt}
    \\usepackage{times}
    \\ifx\\pdfoutput\\undefined
    \\usepackage[ps2pdf,
                pagebackref=true,
                colorlinks=true,
                linkcolor=blue
               ]{hyperref}
    \\usepackage{pspicture}
    \\else
    \\usepackage[pdftex,
                pagebackref=true,
                colorlinks=true,
                linkcolor=blue
               ]{hyperref}
    \\fi
    \\begin{document}
EOS
  end#build_tex_header

  def build_tex_footer()
    return '\end{document}'
  end
end#class
