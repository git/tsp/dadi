#!/bin/bash 


function usage() {
	echo "
Usage: $0 [options]
    -m  tar.gz archive containing the MIBs to be generated
    -t  mib file [excludes -m usage]
    -d  dependency file [excludes -m usage]
    -i  directory where we generate the include files
    -l  directory where we generate the library files
    -f  add the file to archive
    -w  mibs text files destination
    -h  show this message
"
}

[ -z "$DADI_HOME" ] && echo "Env. var DADI_HOME is not set, exiting." && exit -1
source ${DADI_HOME}/dadienv

#
# Command line parsing
#
MIBTGZ=""
EXTRA_FILES=""
DEPENDENCY_FILE=""
MIB_FILES=""
IDEST="include"
LDEST="lib"
while getopts ":m:d:i:l:f:t:d:v:w:" options; do
	case $options in
		m ) MIBTGZ=$OPTARG;;
		i ) IDEST=$OPTARG;;
		l ) LDEST=$OPTARG;;
		t ) MIB_FILES="$MIB_FILES $OPTARG";;
		d ) DEPENDENCY_FILE=$OPTARG;;
		f ) EXTRA_FILES="$EXTRA_FILES $OPTARG";;
		v ) MIB_VERSION=$OPTARG;;
		w ) MIB_DEST=$OPTARG;;
		h ) usage && exit -1;;
		\?) usage && exit -1;;
		* ) usage && exit -1;;
	esac
done

[ -z "$MIB_DEST" ] && MIB_DEST="/usr/share/snmp/mibs"

[ ! -z "$MIB_FILES" -a ! -z "$MIBTGZ" ] && (echo "Cannot use the -t and -m\
				       switch at the same time"; exit -1)

#Create a temp working directory
WDIR=$(mktemp -d /tmp/mibgen.XXXXXX)
[ $? -ne 0 ] && echo "Unable to create a temporary directory in /tmp" && exit -1

if [ ! -z "$MIBTGZ" ]
then
	MIB_VERSION=$(echo $MIBTGZ | perl -n -e 's/.*-(\d+\.\d+).*/\1/;print;')
	# Verify tar.gz files
	[ -z $MIBTGZ ] && echo "Please provide a tar.gz file containing the\
       	MIBs with the -m switch" && exit -1

	tar ztf $MIBTGZ
	[ $? -ne 0 ] && echo "$MIBTGZ is not a valid tar.gz file" && exit -1

	# Untar mibs
	tar zxf $MIBTGZ -C $WDIR || (echo "Could not untar the .tar.gz files in \
	temporary directory" && exit -1)

	MDIR="$WDIR/$(basename $MIBTGZ .tgz)"
fi

# Generate the xml files
[ -z "$DEPENDENCY_FILE" ] && DEPENDENCY_FILE=$MDIR/mibs/dependencies.sh

if [ -f $DEPENDENCY_FILE ] ; then
    source $DEPENDENCY_FILE
else
    ALL_DEPS=$(cd $MDIR/mibs ; /bin/ls *.txt)
    for i in $ALL_DEPS; do
	VAR_NAME=$(echo $i|sed 's/.txt/_DEPS/;s/-/_/g')
	MY_DEPS=$(echo $ALL_DEPS | sed "s/$i//")
 	eval "$VAR_NAME=\"$MY_DEPS\""
    done
fi

XML_FILES=""
[ -z "$MIB_FILES" ] && MIB_FILES=$(find $MDIR/mibs -name "*.txt")

for i in $MIB_FILES
do
    VAR_NAME=$(echo $i|sed 's/.txt/_DEPS/;s/-/_/g;s/.*\///')
    SMIDUMP_SW=
    for dep in ${!VAR_NAME} ; do SMIDUMP_SW="$SMIDUMP_SW -p $dep" ; done
    SMIDUMP_SW="-f xml ${SMIDUMP_SW}"

    XFILE=$(echo $i|sed 's/.txt$/.xml/')
    MYSW=$(echo ${SMIDUMP_SW} | sed -e "s!-p $i!!")
    [ ! -z "$MIBTGZ" ] && cd $MDIR/mibs
    smidump ${MYSW} $i > $XFILE
    [ ! -z "$MIBTGZ" ] && cd "$OLDPWD"
    XML_FILES="$XML_FILES $XFILE"
done

#Call dadi.rb on the generated xml files
${DADI_HOME}/dadi.rb -x $(echo $XML_FILES | sed -e 's/ / -x /g') -d $WDIR

#Copy mib files in WDIR
mkdir ${WDIR}/mibs
cp $MIB_FILES ${WDIR}/mibs

#Copy dadi source files in the WDIR
if [ -d ${DADI_HOME}/src ]
then
	cp ${DADI_HOME}/src/*.c ${WDIR}/src
	cp ${DADI_HOME}/include/*.h ${WDIR}/include/
fi

#Detect if we're invoked from source tree
#in that case, add the missing files
if [ -d ${DADI_HOME}/c ]
then
	cp ${DADI_HOME}/c/*.c ${WDIR}/src
	cp ${DADI_HOME}/c/*.h ${WDIR}/include/
fi

#Copy dadi utils files in the WDIR
cp ${DADI_HOME}/ipc_clean.sh ${WDIR}

#Copy dadi helper files in WDIR
cp ${DADI_HOME}/scripts/mib_gen_helpers/* ${WDIR}

#Copy dadi perl files in the WDIR
mkdir -p ${WDIR}/wrapper/perl5
cp ${DADI_HOME}/wrapper/perl5/*.i ${WDIR}/wrapper/perl5

echo "${DADI_VERSION}_${MIB_VERSION}" > ${WDIR}/VERSION
#Add extra files (-f switch)
[ ! -z "${EXTRA_FILES}" ] && for i in ${EXTRA_FILES}
do
	cp $i ${WDIR}
done

#Create the matching tar file
tar zc --transform "s,^,libdadi-${DADI_VERSION}_${MIB_VERSION}/," \
-C $WDIR -f libdadi-${DADI_VERSION}_${MIB_VERSION}.tar.gz . \
--exclude 'snt-mib*' && rm -rf $WDIR


