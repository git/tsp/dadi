#!/bin/bash
# Generate a package given a git repository

DIR="tmp_package"
mkdir $DIR
cd $DIR
git-clone .. dadi
VERSION=$(echo ${1:-$(git-tag -l | tac | head -n 1)} | sed -e 's/^v//')
echo $VERSION > dadi/VERSION
tar zc --transform "s,^dadi,dadi-${VERSION}/," -f ../dadi-${VERSION}.tgz dadi --exclude .git
cd ../
rm -rf $DIR
