#!/usr/bin/ruby -w
# coverage.rb: tests the coverage of a given mib
#
# Step 1: launch libmib.so:
#     $ ./libmib.so > coverage
# Step 2: use the generated file this way:
#     $ cat coverage | ruby $DADI_HOME/scripts/coverage.rb
# Step 3: analyze the output
# [...]
# pepHttpDnsMaxAgeLoading  RW     # Read and write accessed
# pepDestCounterRxAcmeByte .W     # Write accessed
# pepDestCounterRxTissByte ..     # Not accessed at all
# [...]
#
#
# Copyright (C) 2008 Frederik Deweerdt <frederik.deweerdt@gmail.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program; if not, write to the Free Software Foundation,
# Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

enum_file = File.new("#{ENV[''] == nil ? "." : ENV['DADI_HOME']}/include/enums.h", "r")
id_to_name = Hash.new

enum_file.each_line do
  |l|
  if (l =~ /define (\w+)_id (\d+)/) then
    id_to_name[$2] = $1
  end#if
end#each_line

STDIN.each_line do
  |l|
  if (l =~ /COV (\d+): (\d+)/) then
    id = $1
    access = $2
    access_str = ".."

    if id_to_name[id] == nil
      i = id.to_i

      while i > 0 and id_to_name[i.to_s] == nil do
        i -= 1
      end#while

      if id_to_name[i.to_s] != nil and id_to_name[i.to_s] =~ /Table$/ then
        id_to_name[id] = id_to_name[i.to_s]
      else
        raise "Unknown id #{id} #{access}"
      end#if
    end#if

    if (access.to_i & 1) == 1 then
      access_str[0] = 'R'
    end
    if (access.to_i & 2) == 2 then
      access_str[1] = 'W'
    end
    puts "#{id_to_name[id]} #{access_str}"
  end#if
end#each_line
