# node.rb: holds data definitions
# Copyright (C) 2006,2007  Frederik Deweerdt <frederik.deweerdt@gmail.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program; if not, write to the Free Software Foundation,
# Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

class Node < Hash
	def initialize(name = nil, parent = nil)
		super()
		self["parent"] = parent
		self["name"] = name
	end#initialize

        def to_binding(object = Object.new)
          object.instance_eval("def binding_for(#{keys.join(",")}) binding end")
          object.binding_for(*values)
        end#to_binding
end#class Node

class Notification < Node
end#class Node

class Scalar < Node
end#class Node

class Table < Node
	def initialize(name)
		super(name)
		self["columns"] = []
	end#initialize

	def addCol(c)
		self["columns"] << c
	end#addScalar
end#class Node

class Enum < Node
	def initialize(name)
		self["name"] = name
		self["names"] = Array.new
		self["values"] = Hash.new
	end#initialize

	def add(name, value=nil)
		self["names"]  << name
		self["values"][name] = value
	end#add
	def <=>(other)
		return self["names"] <=> other["names"]
	end#<=>
end#class Enum
