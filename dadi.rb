#!/usr/bin/env ruby
# dadi.rb: main dadi entry point
#
# Copyright (C) 2006,2007,2008
# Frederik Deweerdt <frederik.deweerdt@gmail.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program; if not, write to the Free Software Foundation,
# Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

require 'data_tree.rb'
require 'node.rb'
require 'smi_parser.rb'
require 'optparse'

#
# Options parsing
#

options = {}
op = OptionParser.new do |opts|
  options[:dest] = "."
  options[:root_name] = []
  options[:xml_file] = []
  options[:dumper] = "bb_dumper"
  options[:c_prefix] = ""

  opts.banner = "Usage: dadi.rb [options]"

  opts.on("-x", "--xml-file=FILE", String, "XML file to parse (REQUIRED)", :REQUIRED) do |x|
    options[:xml_file] << x
  end

  opts.on("-p", "--source-prefix=PREFIX",
          String, "Add a prefix to the generated C files") { |v| options[:c_prefix] = v }
  opts.on("-d", "--dest=DEST", String, "Destination directory") { |v| options[:dest] = v }
  opts.on("-f", "--force", "Force overwriting existing files") { |v| options[:force] = v }

  opts.on("-u", "--dumper=MODULE", String, "dumper module to use (default: bb_dumper)", :REQUIRED) {|v| options[:dumper] = v }

  opts.on_tail("-h", "--help", "Show this message") do
    puts opts
    exit
  end
end


#
# Do a first pass on global options
#
begin
  begin
    # Basically, the first parse is here for getting the dumper only
    op.parse(ARGV)
  rescue
    # Ignore parse errors for now, the second parse (below) will handle
    # them
  end
  if options[:xml_file].size < 1
    raise "Parsing error: Missing required arguments"
  end#if
  # Reset the xml_file array for the second parsing
  options[:xml_file] = []

  # Load the approriate class
  begin
    require "#{options[:dumper]}"
  rescue LoadError
    raise "Unable to find the '#{options[:dumper]}' dumper, exiting. (#{$!})"
  end#begin

  # Get a Class instance for dumper
  dumper_class = Object.const_get(options[:dumper].capitalize)

  # Second pass on the options understood by the dumper
  begin
    # Support additional options if needed
    if dumper_class.respond_to?(:add_options)
      dumper_class.add_options(op, options)
    end#if
    op.parse!
  rescue
    puts $!
    puts op
    Kernel.exit
  end#begin
end#begin

#
# Instantiate the appropriate dumper
dumper = dumper_class.new(options)


dest = options[:dest]
if !FileTest.directory?(dest) or !FileTest.writable?(dest)
  puts "Invalid output directory \"#{dest}\", exiting"
  exit(-1)
end#if


def build_dest_folders (dest_dirs, force)
  #
  # Handle destination directories (rmdir them if -f is set)
  #
  begin
    dest_dirs.each { |d| Dir.mkdir(d) }
  rescue
    if force != nil
      dest_dirs.each do |d|
        dirs = []
        Dir.glob("#{d}/**/{.[^.],..?,}*") do |f|
          if File.directory?(f)
            dirs << f
          else
            File.unlink(f)
          end
        end
        dirs.reverse_each(&Dir.method(:rmdir))
      end
    else
      raise "Destination directories exist, use -f to overwrite"
    end
  end
end#build_dest_folders



build_dest_folders(dumper.dest_folders, options[:force]) if dumper.respond_to?(:dest_folders)

dumper.prolog if dumper.respond_to?(:prolog)

types = {}
options[:xml_file].each {
  |xml|

  name = File.basename(xml, ".xml").downcase.gsub("-", "_")

  parser = SmiParser.new(xml, types)
  datatree = parser.parse
  types.update(datatree.types.clone)

  next if datatree.is_empty?

  dumper.dump_datatree(datatree, name)
}

dumper.epilog if dumper.respond_to?(:epilog)
