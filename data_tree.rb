# data_tree: holds a data structure, filled by the parser, used by
# the dumper
#
# Copyright (C) 2006,2007,2008
# Frederik Deweerdt <frederik.deweerdt@gmail.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program; if not, write to the Free Software Foundation,
# Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

require 'node.rb'

class DataTree
  attr_reader :nodes, :tables, :enums, :types , :notifications
  def initialize
    @nodes = []
    @enums = []
    @tables = []
    @notifications = []
    @types = {}
  end#initialize

  def addType(type, basetype)
    @types.store(type, basetype)
  end#addEnum

  def addEnum(enum)
    @enums << enum
  end#addEnum

  def addNode(node)
    @nodes << node
  end#addNode

  def addTable(table)
    @tables << table
  end#addNode

  def addNotification(notification)
    @notifications << notification
  end#addNode

  def to_s
    @nodes.to_s + @enums.to_s + @tables.to_s + @types.to_s + @notifications.to_s
  end#hash

  def is_empty?
    return (@nodes.size == 0 \
            and @tables.size == 0 \
            and @types.keys.size == 0 \
            and @notifications.size == 0)
  end#empty?
end#class DataTree
