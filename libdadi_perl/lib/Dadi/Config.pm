package Dadi::Config;

use DateTime ;
use DateTime::Format::Strptime ;
use Dadi::Config::Loader ;
use Dadi::Config::Saver ;

our $logger=Log::Log4perl->get_logger("Dadi::Config") ;

sub new {
  my ($class, $templatefile, $optionsfile, $conffilespath) = @_ ;

  if ((!defined $conffilespath) ||
      (!defined $templatefile) ||
      (!defined $optionsfile)) {
    $logger->fatal ("usage: __PACKAGE__->new(templatefile, optionsfile,conffilespath)");
    die "usage: __PACKAGE__->new(templatefile, optionsfile,conffilespath)";
  }

  my $self  = {
	       conffilespath => $conffilespath,
	       templatefile => $templatefile,
	       optionsfile => $optionsfile,
	       loader => Dadi::Config::Loader->new,
	       saver  => Dadi::Config::Saver->new ($templatefile)
	      };

  bless $self, $class;
  return $self ;
}

sub load {
  my ($self) = @_ ;

  my $have_to_save ;

  # apply the option fiel
  $self->{loader}->load ($self->{optionsfile}) ;
  # apply the default config
  $self->{loader}->load ($self->{templatefile}) ;

  # find a current config
  if (-f $self->{conffilespath}."current") {
    $self->{loader}->load ($self->{conffilespath}."current") ;
  } elsif (-f $self->{conffilespath}."old_current") {
    $self->{loader}->load ($self->{conffilespath}."old_current") ;
    # there were no current --> rebuild it
    $have_to_save = 1 ;
  } else {
    # make sure we have a current even on first start
    $have_to_save = 1 ;
  }
  if (-f $self->{conffilespath}."have_to_be_applied") {
    $self->{loader}->load ($self->{conffilespath}."have_to_be_applied") ;
    $have_to_save = 1 ;
  }

  $self->save () if ($have_to_save) ;

  # clean useless conf files
  remove_link_and_target ($self->{conffilespath}."old_current") ;
  remove_link_and_target ($self->{conffilespath}."save_failed") ;
  remove_link_and_target ($self->{conffilespath}."have_to_be_applied") ;
}

sub save {
  my ($self) = @_ ;

  my $now = DateTime->now ;
  my $formatter = DateTime::Format::Strptime->new ( pattern =>'%Y-%m-%d_%H-%M-%S') ;
  $now->set_formatter ($formatter);

  $self->{saver}->save_in ($self->{conffilespath}."save_$now.xml") ;

  my $old_current = readlink ($self->{conffilespath}."current") ;
  if (defined $old_current) {
    symlink (File::Spec->rel2abs( $old_current) , $self->{conffilespath}."old_current")
      or die "could not perform symlink : $!";
  }

  unlink ($self->{conffilespath}."current") ;
  symlink (File::Spec->rel2abs( $self->{conffilespath}."save_$now.xml") ,
	   $self->{conffilespath}."current")
    or die "could not perform symlink : $!";

  remove_link_and_target ($self->{conffilespath}."old_current") ;
}

sub activatePending {
  my ($self) = @_ ;

  if (-l $self->{conffilespath}."pending") {
    if (-l $self->{conffilespath}."have_to_be_applied") {
      $logger->info ("Removing previous \"have_to_be_applied\" config file.");
      remove_link_and_target ($self->{conffilespath}."have_to_be_applied");
    }
    $logger->info ("Activating pending config file.");
    rename ($self->{conffilespath}."pending",
	    $self->{conffilespath}."have_to_be_applied");
  }
}

sub currentVersion {
  my ($self) = @_ ;

  my $version="Unknown";

  if (-l $self->{conffilespath}."current") {
    $version = readlink ($self->{conffilespath}."current") ;
    $version =~ s/$self->{conffilespath}// ;
  }

  $logger->info ("Current Version is \"$version\".");

  return $version;
}

sub pendingVersion {
  my ($self) = @_ ;

  my $version="None";

  if (-l $self->{conffilespath}."pending") {
    $version = readlink ($self->{conffilespath}."pending") ;
    $version =~ s/$self->{conffilespath}// ;
  } elsif (-l $self->{conffilespath}."have_to_be_applied") {
    $version = readlink ($self->{conffilespath}."have_to_be_applied") ;
    $version =~ s/$self->{conffilespath}// ;
  }

  $logger->info ("Pending Version is \"$version\".");

  return $version;
}


sub remove_link_and_target {
  my ($link) = @_ ;

  my $target = readlink ($link) ;

  unlink $target if defined $target ;
  unlink $link ;
}


1 ;
