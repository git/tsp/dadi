package Dadi::UniqFork ;

use strict ;
use POSIX ":sys_wait_h" ;
use Log::Log4perl ;

our $logger=Log::Log4perl->get_logger("Dadi::UniqFork") ;

our $FORKER_BY_NAME = {};
our $FORKER_BY_PID = {};

sub catch_sigchild{
  foreach my $pid (keys %$FORKER_BY_PID) {
    if (($pid = waitpid ($pid, WNOHANG)) > 0) {
      $logger->debug ("search forker for pid ",$pid) ;
      my $forker = $FORKER_BY_PID->{$pid};
      if (!$forker) {
	$logger->fatal ("forker not found for $pid") ;
	return ;
      }
      $forker->{pid}=0
    }
  }
}

$SIG{CHLD} = \&catch_sigchild ;

sub _new {
  my($class, $name) = @_;

  if (!defined $name) {
    $logger->fatal ("usage: __PACKAGE__->_new(name)");
    die "usage: __PACKAGE__->_new(name)";
  }

  $logger->debug ("$class/$name");

  # Have we created it previously?
  if (exists $FORKER_BY_NAME->{$name}) {
    $logger->debug ("$name exists already");
    return $FORKER_BY_NAME->{$name};
  }

  my $self  = {
	       pid  => 0,
	       name => $name,
	      };

  bless $self, $class;

  # Save it in global structure
  $FORKER_BY_NAME->{$name} = $self;

  return $self;
}



##################################################
sub get_forker {
##################################################
    my($class, $name) = @_;

    $logger->debug ("\$class $class, \$name $name");
    my $forker = $class->_new($name);
    return $forker;
}

sub launch_and_die {
  my ($self, $func, $args) = @_;

  $logger->debug ("try to launch ", $self->{name}, "with actual pid ",
		  $self->{pid});

  if ($self->{pid}) {
    $logger->warn ("There's already a pending \"", $self->{name},"\" running");
    return undef ;
  }

  $self->{pid} = fork ;
  if ($self->{pid} == 0) {
    &$func(@$args);
    $logger->debug ("func of [",$self->{name},
		    "] has returned ... exit ....: ");
    exit 0 ;
  }

  $logger->debug ("launched ", $self->{name}, "with pid ",$self->{pid});
  $FORKER_BY_PID->{$self->{pid}} = $self ;

  return 1;
}
