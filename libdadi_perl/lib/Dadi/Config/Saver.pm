package Dadi::Config::Saver;


use strict ;
use Log::Log4perl ;
use XML::LibXML;

use Dadi::Config::Core;

our $logger=Log::Log4perl->get_logger("Dadi::Config::Saver") ;

sub new {
  my ($class, $xmltemplatefile) = @_ ;

  if (!defined $xmltemplatefile) {
    $logger->fatal ("usage: __PACKAGE__->new(xmltemplatefile)");
    die "usage: __PACKAGE__->new(xmltemplatefile)";
  }

  $logger->debug ("$class/$xmltemplatefile");

  my $parser = XML::LibXML->new();
  my $doc ;
  eval ('$doc = $parser->parse_file( $xmltemplatefile );');
  if (!$doc) {
    $logger->error("could not parse $xmltemplatefile: $!");
    return undef ;
  }
  my $param_nodes = $doc->findnodes('//*[@value]');
  my $self  = {
	       doc  => $doc,
	       param_nodes => $param_nodes,
	      };

  bless $self, $class;
  return $self ;
}

sub save_in {
  my ($self, $filename) = @_;

  foreach my $node (@{$self->{param_nodes}}) {
    my $getter_name = find_symbol_from_node($node);
    $getter_name =~ s/::/::get_/ ;

    my $index_string = "" ;
    my $index = $node->parentNode->getAttribute ('index') ;

    if (defined $index) {
      $index_string = $index ;
    }

    $logger->debug ( "eval \$value = $getter_name ($index_string)");

    my $value ;
    eval "\$value = $getter_name ($index_string)";
    if ($@) {
      $logger->warn ("parameter \"", $node->nodeName, "\" not applied : $@") ;
      next ;
    }

    $node->setAttribute ('value', $value);
  }
  $self->{doc}->toFile ($filename) ;
}

1;
