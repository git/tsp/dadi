package Dadi::Config::Loader;

use strict ;
use Log::Log4perl ;
use XML::LibXML;

use Dadi::Config::Core;

our $logger=Log::Log4perl->get_logger("Dadi::Config::Loader") ;

sub new {
  my ($class) = @_ ;

  $logger->debug ("$class");

  my $self  = {};

  bless $self, $class;
  return $self ;
  }


sub load {
  my ($self, $xmlfilename) = @_ ;

  $logger->debug("$xmlfilename");

  my $parser = XML::LibXML->new();
  my $doc ;
  eval ('$doc = $parser->parse_file( $xmlfilename );');
  if (!$doc) {
    $logger->error("could not parse $xmlfilename: $!");
    return undef ;
  }

  my $nodelist = $doc->findnodes('//*[@value]');

  foreach my $node (@$nodelist) {
    my $setter_name = find_symbol_from_node($node);
    $setter_name =~ s/::/::set_/ ;

    my $value = $node->getAttribute('value');

    my $index_string = "" ;
    my $index = $node->parentNode->getAttribute ('index') ;

    if (defined $index) {
      $index_string = $index . "," ;
    }

    $logger->debug ("  eval \"$setter_name ($index_string $value)\"");

    eval "$setter_name ($index_string \$value)";
    if ($@) {
      $logger->warn ("parameter \"", $node->nodeName, "\" not applied : $@") ;
      next ;
    }
  }
}

1;
