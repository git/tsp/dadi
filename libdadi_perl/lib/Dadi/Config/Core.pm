package Dadi::Config::Core;


use strict ;

use vars qw(@ISA @EXPORT);
use Exporter;
@ISA = qw(Exporter);
@EXPORT = qw(&find_symbol_from_node);

sub find_symbol_from_node {
  my ($node) = @_;

  my $type = $node->getAttribute ('type') ;

  my $param_name=$node->nodeName ;
  return "$param_name"."::" if defined $type && $type eq "MIB";
  $param_name =~ s/^_// ;
  return find_symbol_from_node ($node->parentNode).$param_name ;
}

1;
