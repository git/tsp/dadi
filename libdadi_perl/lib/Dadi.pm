package Dadi;

use strict ;
use Log::Log4perl qw(:levels);

use perldadi ;

our $logger=Log::Log4perl->get_logger("Dadi") ;

our $dadi ;

##################################################
sub get_instance {
    my($class) = @_;

    if (!$dadi) {
      $dadi = {
	       get_methods  => [],
	       set_methods  => [],
	      };

      bless $dadi, $class;

      $logger->info("Registering the dadi callback and init");
      perldadi::dadi_first_init (\&dadi_callback);
      perldadi::dadi_set_loging_method(\&dadi_log_callback);

    }

    return $dadi;
}

sub dadi_callback {
  my ($var, $event_type) = @_ ;

  $logger->debug ("(\$var $var, \$event_type $event_type)");

  if ($event_type == $perldadi::DADI_EVENT_TYPE_GET) {
    if ($dadi->{get_methods}[$var]) {
      foreach (@{$dadi->{get_methods}[$var]}) {
	return 0 unless &$_($var) ;
      }
      return 1;
    }
  } elsif ($event_type == $perldadi::DADI_EVENT_TYPE_SET) {
    if ($dadi->{set_methods}[$var]) {
      foreach (@{$dadi->{set_methods}[$var]}) {
	return 0 unless &$_($var) ;
      }
      return 1;
    }
  } else {
    $logger->error ("bad event type $event_type");
  }

  return 0 ;
}

my @syslog_2_log4perl = ( $FATAL,   # LOG_EMERG
			  $ERROR,   # LOG_ALERT
			  $ERROR,   # LOG_CRIT
			  $ERROR,   # LOG_ERR
			  $WARN,    # LOG_WARNING
			  $INFO,    # LOG_NOTICE
			  $INFO,    # LOG_INFO
			  $DEBUG ); # LOG_DEBUG

sub dadi_log_callback {
  my ($prio, $file, $line, $function, $formated_string) = @_ ;

  if ($prio < 0 || $prio > 7) {
    $logger->error("Bad prio value ($prio)");
    $prio = 7;
  }

  $file =~ s!.*/!!; # remove complete path !!!

  $logger->log($syslog_2_log4perl[$prio],
	       "$file:$line ($function) $formated_string");
}

sub add_getter_handler  {
  my ($self, $id, $getter) = @_;
  $self->{get_methods}[$id] = [] unless $self->{get_methods}[$id];
  push(@{$self->{get_methods}[$id]}, $getter);
}

sub add_setter_handler  {
  my ($self, $id, $setter) = @_;

  $logger->debug("\$id $id");

  $self->{set_methods}[$id] = [] unless $self->{set_methods}[$id];
  push (@{$self->{set_methods}[$id]},$setter);

}

sub set_log_method {
  my ($self, $logger) = @_;
  perldadi::dadi_set_loging_method($logger);
}


sub run {
  my $self = @_ ;

  $logger->info("Starting dadi mainloop ...");
  perldadi::dadi_mainloop ();
}

sub stop_request {
  my $self = @_ ;

  $logger->info();
  perldadi::dadi_terminate_request ();
}

sub clean {
  my $self = @_ ;

  $logger->info();
  perldadi::dadi_stop ();
}

1;
