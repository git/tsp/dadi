#include <net-snmp/net-snmp-config.h>
#include <net-snmp/net-snmp-includes.h>
#include <net-snmp/agent/net-snmp-agent-includes.h>

#include <stddef.h>
#include <unistd.h>

#include "dadi.h"
#include "inlines_mib.h"
int test_callback(int type, enum dadi_event_type dadi_event_type)
{
	if (dadi_event_type == DADI_EVENT_TYPE_GET) {
		printf("Get done : %d\n", type);
	} else {
		printf("Set done : %d %d\n", type, dadi_event_type);
	}
	return 1;
}

int main(void)
{
	dadi_first_init(test_callback);
	set_sntMacConfigUseGPSModule(0xab);
	set_sntMacConfigNcrOffset(0xab);
	dadi_mainloop(0);

	return 0;
}

