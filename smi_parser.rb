# smi_parser.rb: parses a smidump'ed MIB
# Copyright (C) 2006,2007  Frederik Deweerdt <frederik.deweerdt@gmail.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program; if not, write to the Free Software Foundation,
# Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

require "rubygems"
require "xml/libxml"

# As of ruby 1.8.6, the Range.max method is extremly slow probably because
# it's supposed to work with other classes than just integer this little
# wrapper class saves minutes(!) when generating code for intergers whose
# ranges are large
class DadiRange < Range
  def initialize(min, max)
    super(min, max)
    @_min = min
    @_max = max
  end#initialize

  def min
    return @_min
  end#min

  def max
    return @_max
  end#max
  def length
    return (max - min)+1
  end#length
end#class DadiRange

class SmiParser
  def initialize(filename, types)
    @filename = filename
    begin
      parser = XML::Parser.file(@filename)
      @doc = parser.parse
    rescue
      raise "Cannot parse #{filename}:#{$!}"
    end
    @datatree = DataTree.new
    @type_mapper = { "Integer32" => "int",
        "Unsigned32" => "uint",
        "Gauge32" => "uint",
        "Counter32" => "counter32",
        "TruthValue" => "int",
        "enum" => "int",
        "OctetString" => "chartab",
        "DisplayString" => "chartab",
        "IpAddress" => "ipaddr",
        "MacAddress" => "macaddr" ,
        "InetAddressType" => "int" ,
        "InetAddress" => "chartab" ,
        "ObjectIdentifier" => "chartab" ,
        "InetPortNumber" => "uint" ,
        "IPv6FlowLabel" => "uint" ,
        "IPv6FlowLabelOrAny" => "int" ,
        "InterfaceIndexOrZero" => "int" ,
        "RowStatus" => "int" ,
        "StorageType" => "int" ,
        "IANAifType" => "uint" ,
        "IANAtunnelType" => "uint" ,
        "CounterBasedGauge64" => "uint64_t" ,
        "Counter64" => "uint64_t" }

    # Add previously parsed types
    @type_mapper.update(types) if types != nil

    @type_sizer = { "MacAddress" => 6,
        "int" => "sizeof(int)",
        "uint" => "sizeof(unsigned int)",
        "Counter32" => "sizeof(unsigned int)",
        "DisplayString" => 255,
        "OctetString" => 255,
        "IpAddress" => 4,
        "InetAddressType" => 4,
        "InetAddress" => 255,
        "ObjectIdentifier" => 255,
        "InetPortNumber" => 4,
        "CounterBasedGauge64" => "sizeof(uint64_t)" ,
        "Counter64" => "sizeof(uint64_t)" }
  end#initialize

  def parseRange(el_range)
    el_range.find_first("syntax").each_element do
      |e|
      case e.name
        when "typedef"
          rel = e.find_first("range")
          if rel != nil then
            return DadiRange.new(rel.attributes["min"].to_i,
                     rel.attributes["max"].to_i)
          end#if
      end#case
    end#each_element
    return nil
  end#parseRange

  def parseTable(el_table)
    t = Table.new(el_table.attributes["name"].sub("-", "_"))
    t["oid"] = el_table.attributes["oid"]
    index_name = ""
    el_table.find("row/linkage/index").each { |e|
      index_name = e.attributes["name"]
    }
    el_table.find("row/column").each do
      |e|
      if e.attributes["name"] == index_name
      then
        r = parseRange(e)
        t["index_name"] = index_name
        t["range"] = r
        t["length"] = r.length
        next
      end#if
      s = parseScalar(e)
      t.addCol(s)
    end#each
    return t
  end#parseTable

  def parseScalar(xml_scalar)
    s = Scalar.new(xml_scalar.attributes["name"].sub("-", "_"))
    s["range"] = parseRange(xml_scalar)
    s["length"] = s["range"].length if s["range"] != nil
    s["oid"] =  xml_scalar.attributes["oid"]
    s["access"] = xml_scalar.attributes["access"] if xml_scalar.attributes["access"] != nil
    s["status"] =  xml_scalar.attributes["status"]

    xml_scalar.find("syntax").each do
      |el|
      begin
      el.each_element do
        |ele|
        case ele.name
          when "typedef"
            ret = parseTypedef(ele, s["name"])
            if ret != nil then
              s["type"] = @type_mapper.fetch(ret)
              s["real_type"] = "#{ret}"
            else
              s["type"] = @type_mapper.fetch("enum")
              s["real_type"] = "int"
            end#if
          when "type"
            s["type"] = @type_mapper.fetch(ele.attributes["name"])
            s["real_type"] = ele.attributes["name"]
          else
            raise "Unknown SYNTAX element #{ele.to_s}"
        end#case
      end#each_element
      rescue
            raise "Unknown SYNTAX element #{el}"
      end
    end#each

    if xml_scalar != nil then
      s["access"] = xml_scalar.find_first("access").content
    end#if
    if xml_scalar.find_first("description") != nil then
      s["description"] = xml_scalar.find_first("description").content.strip.tr("\n","").squeeze(" ")
    end#if
    if xml_scalar.find_first("default") != nil then
      s["default"] = xml_scalar.find_first("default").content
    end#if
    if xml_scalar.find_first("units") != nil then
      s["units"] = xml_scalar.find_first("units").content
    end#if

    if s["type"] != nil then
      s["size"] = @type_sizer[s["real_type"]]
      s["size"] = 255 if s["size"] == nil and s["type"] == "chartab"
      s["length"] = @type_sizer[s["real_type"]] if s["length"] == nil
      s["length"] = 255 if s["length"] == nil and s["type"] == "chartab"
    end#if
    return s
  end#parseScalar

  def parseTypedefs(el_typedefs)
    el_typedefs.each_element do
      |e|
      parseTypedef(e, nil)
    end#each_element
  end#parseTypedefs

  def parseTypedef(typedef_tag, class_name = nil)
      basetype = typedef_tag.attributes["basetype"]
      if class_name == nil then
        name = typedef_tag.attributes["name"]
      else
        name = class_name
      end#if

      case basetype
        when "Enumeration"
          # has a parent? If yes fetch its properties
          if typedef_tag.find_first("parent") != nil then
            # Fetch parent
            eparent = typedef_tag.find_first("parent")
            p_base_type = eparent.attributes["name"]
            # Get C representation
            p_real_type  = @type_mapper[p_base_type]
            # Update type mapper
            @type_mapper[name] = p_real_type
            @datatree.addType(name, p_real_type)
          else
            enum = Enum.new(name)
            typedef_tag.find("namednumber").each do
              |el|
              enum.add(el.attributes["name"], el.attributes["number"])
            end#each
            @datatree.addEnum(enum)
          end#if
          return nil
        else
          if not @type_mapper.has_key? name then
            @type_mapper[name] = basetype
            @datatree.addType(name, @type_mapper[basetype]) if basetype != nil
          end#if

          if @type_mapper.has_key? basetype and class_name != nil then
            return basetype
          end#if
      end#case
  end#parseTypedef

  def parseNodes(el_node)
    el_node.each_element do
      |e|
      case e.name
        when "scalar"
          @datatree.addNode(parseScalar(e))
        when "node"
        when "table"
          t = parseTable(e)
          @datatree.addTable(t)
        else
          raise "Unknown node #{e.name}"
      end#case
    end#each_element
  end#parseNodes

  def parseNotifications(el_notifs)
    el_notifs.each_element do
      |e_notif|
      notif = Notification.new(e_notif.attributes["name"])
      notif["oid"] =  e_notif.attributes["oid"]
      notif["objects"] = []
      e_notif.find_first("objects").each_element do
        |e_obj|
        h = {}
        h["name"] = e_obj.attributes["name"]
        notif["objects"] << h
      end#each_element
      @datatree.addNotification(notif)
    end#each_element
  end#parseNotifications
  def parse()
    begin
    @doc.root().each_element do
      |e|
      case e.name
        when "typedefs"
          parseTypedefs(e)
        when "nodes"
          parseNodes(e)
        when "notifications"
          parseNotifications(e)
        else
      end#case
    end#each_element
    return @datatree
#    rescue
#      raise "Cannot parse #{@filename}:#{$!}"
    end
  end#parse
end#class SmiParser
