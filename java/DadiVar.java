package org.nongnu.dadi;

public class DadiVar
{
	public String name;
	public String oid;
	public String type;
	public String access;
	public String desc;
	public String value;
	public String range;
	public String defval;
	public String units;
	public String group;
	public String ui;
	public DadiVar(String name, String oid, String type,
		       String access, String desc, String value,
		       String range, String defval, String units,
		       String group, String ui) {
		this.name = name;
		this.oid = oid;
		this.type=type;
		this.access=access;
		this.desc=desc;
		this.value=value;
		this.range=range;
		this.defval=defval;
		this.units=units;
		this.group=group;
		this.ui=ui;
	}
}

