#include <net-snmp/net-snmp-config.h>
#include <net-snmp/net-snmp-includes.h>
#include <string.h>
#include <stdlib.h>
#include <alloca.h>
#include <fcgi_stdio.h>

netsnmp_session *snmp_init(void)
{
	netsnmp_session session, *ss;

	init_snmp("dadicgi");

	snmp_sess_init(&session);

	session.peername = "localhost";
	session.version = SNMP_VERSION_1;
	session.community = "public";
	session.community_len = strlen(session.community);

	SOCK_STARTUP;
	ss = snmp_open(&session);

	return ss;
}

#define debug(x...) do {\
		FILE *f = fopen("/tmp/aze.log", "a+"); \
		fprintf(f, x);\
		fclose(f);\
	} while(0);
char *snmpset(netsnmp_session *ss, char *oid_str, char type, char *value)
{
	struct snmp_pdu *pdu;
	struct snmp_pdu *response;

	oid anOID[MAX_OID_LEN];
	size_t anOID_len = MAX_OID_LEN;
	void *dataptr = NULL;
	int status;
	char *ret;
	unsigned int value_integer;

	pdu = snmp_pdu_create(SNMP_MSG_SET);
	if (!snmp_parse_oid(oid_str, anOID, &anOID_len)) {
		return strdup("Cannot parse OID");
	}

	if (type == 'i') {
		value_integer = strtoul(value, NULL, 0);
		dataptr = &value_integer;
	} else if (type == 's') {
		dataptr = value;
	} else {
		dataptr = NULL;
	}

	status = snmp_add_var(pdu, anOID, anOID_len, 's', dataptr);
	if (status != SNMPERR_SUCCESS) {
		const char *errstr = snmp_errstring(status);
		ret = malloc(strlen(errstr) + 1024);
		sprintf(ret, "request error: %s %d", errstr, status);
		return ret;
	}

	status = snmp_synch_response(ss, pdu, &response);
	if (status == STAT_SUCCESS && response->errstat == SNMP_ERR_NOERROR) {
		ret = strdup(value);
	} else {
		const char *errstr = snmp_errstring(response->errstat);
		ret = malloc(strlen(errstr) + 17);
		sprintf(ret, "request error: %s\n", errstr);
	}

	if (response)
		snmp_free_pdu(response);

	return ret;
}

char *snmpget(netsnmp_session *ss, char *oid_str)
{
	netsnmp_pdu *pdu;
	netsnmp_pdu *response;
	oid anOID[MAX_OID_LEN];
	size_t anOID_len;
	netsnmp_variable_list *vars;
	int status;
	char *ret = NULL;


	pdu = snmp_pdu_create(SNMP_MSG_GET);
	anOID_len = MAX_OID_LEN;
	if (!snmp_parse_oid(oid_str, anOID, &anOID_len)) {
		return strdup("Cannot parse OID");
	}
	snmp_add_null_var(pdu, anOID, anOID_len);

	status = snmp_synch_response(ss, pdu, &response);

	if (status == STAT_SUCCESS && response->errstat == SNMP_ERR_NOERROR) {
		for(vars = response->variables; vars; vars = vars->next_variable) {
			char *sp;
			switch(vars->type) {
			case ASN_OCTET_STR:
				sp = (char *)malloc(1 + vars->val_len);
				memcpy(sp, vars->val.string, vars->val_len);
				sp[vars->val_len] = '\0';
				ret = sp;
				goto out;
			case ASN_INTEGER:
			case ASN_COUNTER:
			case ASN_UNSIGNED:
				sp = (char *)malloc(128);
				sprintf(sp, "%ld", *vars->val.integer);
				ret = sp;
				goto out;
			default:
				break;
			}
		}
	} else {
		if (status == STAT_SUCCESS) {
			const char *errstr = snmp_errstring(response->errstat);
			ret = malloc(strlen(errstr) + 17);
			sprintf(ret, "request error: %s\n", errstr);
		} else if (status == STAT_TIMEOUT)
			ret = strdup("timeout");
		else {
			snmp_sess_perror("snmpdemoapp", ss);
		}

	}

out:
	if (response)
		snmp_free_pdu(response);
	return ret;
}

#define OID_VARIABLE "oid="
#define SET_OID_VARIABLE "soid="
#define VAL2SET_VARIABLE "value="
#define SNMP_TYPE_VARIABLE "type="

char *do_get(netsnmp_session *ss, char *data, char **str_oid)
{
	int is_snmp_set = 0;
	char *value = NULL;

	if (!strstr(data, SET_OID_VARIABLE)) {
		if (!strstr(data, OID_VARIABLE)) {
			goto err;
		}
	} else {
		is_snmp_set = 1;
	}

	*str_oid = malloc(strlen(data)+1);
	if (!*str_oid)
		goto err;

	if (is_snmp_set) {
		char *tok;
		char *value_to_set = NULL;
		char snmp_type = '\0';

		tok = strtok(data,"&");
		while (tok != NULL) {
			if (!strncmp(tok, SET_OID_VARIABLE, 5)) {
				if(sscanf(tok, SET_OID_VARIABLE "%s", *str_oid) != 1) {
					(*str_oid)[0] = '\0';
					goto err;
				}
			} else if (!strncmp(tok, VAL2SET_VARIABLE, 6)) {
				value_to_set = alloca(strlen(tok)+1);
				if(sscanf(tok,VAL2SET_VARIABLE "%s", value_to_set) != 1) {
					(*str_oid)[0] = '\0';
					goto err;
				}
			} else if (!strncmp(tok, SNMP_TYPE_VARIABLE, 5)) {
				snmp_type = tok[5];
			}
			tok = strtok(NULL,"&");
		}
		if (!(*str_oid)[0] || !value_to_set || !snmp_type) {
			goto err;
		}

		value = snmpset(ss, *str_oid, snmp_type, value_to_set);
	} else {
		if(sscanf(data, OID_VARIABLE "%s", *str_oid) != 1) {
			(*str_oid)[0] = '\0';
			goto err;
		}

		value = snmpget(ss, *str_oid);
	}

err:
	return value;
}

int main(void)
{
	netsnmp_session *ss;
	char err_resp[] = "Unable to process SNMP request";

	ss = snmp_init();
	while (FCGI_Accept() >= 0) {
		char *str_oid = NULL;
		char *value = NULL;
		char *data;
		printf("Content-type: text/html\n\n");

		data = getenv("QUERY_STRING");
		if (!data)
			goto err;

		value = do_get(ss, data, &str_oid);

err:
		if (!value)
			value = err_resp;

		printf("{ \"%s\": \"%s\", \"%s\": \"%s\"}\n", "return_value", value ? value : "null"
							    , "oid", str_oid ? str_oid : "null");

		if (value != err_resp)
			free(value);

		free(str_oid);
	}

	snmp_close(ss);
	SOCK_CLEANUP;

	return 0;
}
