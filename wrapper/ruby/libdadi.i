/**/
%module libdadi_ruby
%{
#include <stdio.h>
#include "dadi.h"
#include "enums.h"
#include "inlines_mib.h"

VALUE ruby_dadi_callback = 0;

int c_dadi_callback(int var,void *data,int size){

	VALUE ruby_ret ;
	int ret;

	fprintf (stderr, "call with %d %p %d\n",var,data,size);


	ruby_ret = rb_funcall (ruby_dadi_callback, rb_intern("call"), 3,
			       INT2NUM (var),
			       SWIG_NewPointerObj(SWIG_as_voidptr(data),
						  SWIGTYPE_p_void, 0|0 ),
			       INT2NUM (size));
	ret = NUM2INT(ruby_ret) ;
	fprintf (stderr, "returned %d\n", ret);
	return ret;
}


char * to_string (void * data, int size)
{
	if (strlen ((char *)data) != size) {
		fprintf(stderr, "%s: Bad string size\n", __func__);
		exit (1) ;
	}
	return (char *)data ;
}

int to_int (void * data, int size)
{
	if (sizeof(int) != size) {
		fprintf (stderr, "%s: Bad int size\n", __func__);
		exit (1) ;
	}
	return *(int *)data ;
}
%}


#pragma SWIG nowarn=801 /* Warning(801): Wrong class name */

%typemap(in) dadi_callback_t {
	ruby_dadi_callback = $input ;
	$1 = c_dadi_callback ;
 }

%include "dadi.h"
%include "enums.h"
%include "inlines_mib.h"

char * to_string (void * data, int size);
int    to_int (void * data, int size);

