/**/
%module libmib_perl
%{
#include <stdio.h>
#include "dadi.h"
#include "enums.h"
#include "inlines_mib.h"

static SV* perl_dadi_callback = NULL;

#if SWIG_VERSION < 0x010331
#define ENTER push_scope()
#define SWIG_as_voidptr(a) (void *)((const void *)(a)) 
#endif

int c_dadi_callback(int var,enum dadi_event_type event_type){
	int ret ;

	dSP;

	ENTER;
	SAVETMPS;
	PUSHMARK(sp);

	XPUSHs(sv_2mortal(newSViv(var)));
	XPUSHs(sv_2mortal(newSViv(event_type)));

	PUTBACK;
	perl_call_sv(SvRV(perl_dadi_callback),G_EVAL|G_SCALAR);
	SPAGAIN;
	ret=POPi;

	PUTBACK;
	FREETMPS;
	LEAVE;
	return ret;
}
%}


%typemap(in) dadi_callback_t {
	if (!SvROK($input) || (SvTYPE(SvRV($input)) != SVt_PVCV)) {
		croak("Expected a function reference");
	}
	if (perl_dadi_callback != NULL)
		SvREFCNT_dec(perl_dadi_callback);
	perl_dadi_callback = SvREFCNT_inc($input) ;
	$1 = c_dadi_callback ;
 }

%typemap(in) IpAddress {
	IpAddress ipaddr ;
	STRLEN len ;
	char * str ;
	int i ;
	char *strtok_saveptr ;
	char * token ;

	if (!SvPOK($input)) {
		croak("Expected a string");
	}

	str = strdup (SvPV($input, len)) ;
	token = strtok_r (str, ".", &strtok_saveptr) ;
	i = 0 ;
	while (token != NULL) {
		long int token_value ;
		char *endptr = NULL ;
		if (i > 3){
			free (str) ;
			croak("Bad ip address \"%s\"", SvPV($input, len));
		}
		token_value = strtol (token, &endptr, 10) ;
		if (*endptr != '\0') {
			free (str) ;
			croak("Bad ip address \"%s\"", SvPV($input, len));
		}
		if ((token_value < 0) || (token_value > 255)) {
			free (str) ;
			croak("Bad ip address \"%s\"", SvPV($input, len));
		}
		ipaddr[i++] = token_value ;
		token = strtok_r (NULL, ".", &strtok_saveptr) ;
	}
	if (i!=4) {
		free (str) ;
		croak("Bad ip address \"%s\"", SvPV($input, len));
	}
	free (str) ;

	$1 = ipaddr ;
 }

%typemap(out) IpAddress * {
	char str[16] ;
	sprintf (str, "%d.%d.%d.%d",
		 (*$1)[0], (*$1)[1], (*$1)[2], (*$1)[3]) ;
	$result = SWIG_FromCharPtr(str);
	argvi++;
 }


%typemap(in) MacAddress {
	MacAddress macaddr ;
	STRLEN len ;
	char * str ;
	int i ;
	char *strtok_saveptr ;
	char * token ;

	if (!SvPOK($input)) {
		croak("Expected a string");
	}

	str = strdup (SvPV($input, len)) ;
	token = strtok_r (str, ":", &strtok_saveptr) ;
	i = 0 ;
	while (token != NULL) {
		long int token_value ;
		char *endptr = NULL ;
		if (i > 5){
			free (str) ;
			croak("Bad mac address %s (too much token)",
			      SvPV($input, len) );
		}
		token_value = strtol (token, &endptr, 16) ;
		if (*endptr != '\0') {
			free (str) ;
			croak("Bad mac address %s (bad formated token %s)",
			      SvPV($input, len) , token);
		}
		if ((token_value < 0) || (token_value > 255)) {
			free (str) ;
			croak("Bad mac address %s (bad range token %s)",
			      SvPV($input, len),token );
		}
		macaddr[i++] = token_value ;
		token = strtok_r (NULL, ":", &strtok_saveptr) ;
	}
	if (i!=6) {
		free (str) ;
		croak("Bad mac address %s (bad formated token %s)",
		      SvPV($input, len),token );
	}

	free (str) ;

	$1 = macaddr ;
 }

%typemap(out) MacAddress * {
	char str[16] ;
	sprintf (str, "%02X:%02X:%02X:%02X:%02X:%02X",
		 (*$1)[0], (*$1)[1], (*$1)[2], (*$1)[3], (*$1)[4], (*$1)[5]) ;
	$result = SWIG_FromCharPtr(str);
	argvi++;
 }



%include "dadi.h"
%include "enums.h"
%include "inlines_mib.h"


