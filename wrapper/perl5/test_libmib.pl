
use strict  ;
use libmib_perl ;


my @get_methods ;
my @set_methods ;

sub dadi_callback {
  my ($var, $data, $size) = @_ ;

  if ($size == 0) {		# get functions
    if ($get_methods[$var]) {
      return $get_methods[$var]->function ($var) ;
    }
  } else {			# set functions
    if ($set_methods[$var]) {
      my $arg = $set_methods[$var]->{change_type}($data, $size) ;
      return $set_methods[$var]->{function} ($var, $arg);
    }
  }

  return 0 ;
}



sub setConfigNetworkConfigFileDownloadUrl {
  my ($var,$string) = @_;

  print "setConfigNetworkConfigFileDownloadUrl called with \$string [$string]\n"
}

$set_methods[$libmib_perlc::rcstConfigNetworkConfigFileDownloadUrl_id]
  = {
     function    => \&setConfigNetworkConfigFileDownloadUrl,
     change_type => \&libmib_perlc::to_string,
    } ;


sub setConfigNetworkOamIpAddrAssign {
    my ($var,$value) = @_;

    print "setConfigNetworkOamIpAddrAssign with \$value [$value]\n"
}

$set_methods[$libmib_perlc::rcstConfigNetworkOamIpAddrAssign_id]
  = {
     function    => \&setConfigNetworkOamIpAddrAssign,
     change_type => \&libmib_perlc::to_int,
    } ;


libmib_perlc::dadi_first_init (\&dadi_callback);

libmib_perlc::set_rcstConfigNetworkOamIpAddrAssign (12345) ;

libmib_perlc::dadi_mainloop ();
